package com.project.core.common.message;

public interface Identifiable<T> {
    T getId();
}
