package com.project.core.common.message;

public interface InstaceBuilder<T> {
    T build();
}
