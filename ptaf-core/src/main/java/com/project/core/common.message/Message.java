package com.project.core.common.message;

import java.time.Instant;

import static java.util.Objects.requireNonNull;

public interface Message extends Identifiable<String> {

    String getFromParticipantId();

    String getToParticipantId();

    String getReplyToQueue();

    String getReplyToQueueManager();

    String getMessageId();

    String getCorrelationId();

    String getBusinessCorrelationId();

    Object getPayload();

    final class Builder implements InstaceBuilder<Message> {

        private DefaultMessage msg;

        public Builder(final String id, final Object payload) {
            setId(requireNonNull(id));
            setPayload(requireNonNull(payload));
        }

        public Builder(final Message base) {
            if (base == null) {
                return;
            }

            setId(base.getId());
            setToParticipantId(base.getToParticipantId());
            setFromParticipantId(base.getFromParticipantId());
            setCorrelationId(base.getCorrelationId());
            setBussinessCorrelationId(base.getBusinessCorrelationId());
            setMessageId(base.getMessageId());
            setReplyToQueue(base.getReplyToQueue());
            setReplyToQueueManager(base.getReplyToQueueManager());
            setPayload(base.getPayload());
        }

        public Builder setId(final String id) {
            getMsg().id = id;
            return this;
        }

        public Builder setPayload(final Object payload) {
            getMsg().payload = payload;
            return this;
        }

        public Builder setToParticipantId(final String toParticipantId) {
            getMsg().toParticipantId = toParticipantId;
            return this;
        }

        public Builder setFromParticipantId(final String fromParticipantId) {
            getMsg().fromParticipantId = fromParticipantId;
            return this;
        }

        public Builder setCorrelationId(final String correlationId) {
            getMsg().correlationId = correlationId;
            return this;
        }

        public Builder setBussinessCorrelationId(final String bussinessCorrelationId) {
            getMsg().businessCorrelationId = bussinessCorrelationId;
            return this;
        }

        public Builder setMessageId(final String messageId) {
            getMsg().messageId = messageId;
            return this;
        }

        public Builder setReplyToQueue(final String replyToQueue) {
            getMsg().replyToQueue = replyToQueue;
            return this;
        }

        public Builder setReplyToQueueManager(final String replyToQueueManager) {
            getMsg().replyToQueueManager = replyToQueueManager;
            return this;
        }


        @Override
        public Message build() {
            final Message ret = getMsg();
            msg = null;
            return ret;
        }

        private DefaultMessage getMsg() {
            DefaultMessage ret = msg;
            if (ret == null) {
                ret = new DefaultMessage();
                msg = ret;
            }
            return ret;
        }


        private static final class DefaultMessage implements Message {
            private Long safEntryTime;
            private String id;
            private Object payload;
            private String fromParticipantId;
            private String toParticipantId;
            private String replyToQueue;
            private String replyToQueueManager;
            private String correlationId;
            private String businessCorrelationId;
            private String messageId;
            private Instant expiration;
            private int attempt;

            @Override
            public String getId() {
                return id;
            }

            @Override
            public Object getPayload() {
                return payload;
            }

            @Override
            public String getFromParticipantId() {
                return fromParticipantId;
            }

            @Override
            public String getToParticipantId() {
                return toParticipantId;
            }

            @Override
            public String getReplyToQueue() {
                return replyToQueue;
            }

            @Override
            public String getReplyToQueueManager() {
                return replyToQueueManager;
            }

            @Override
            public String getCorrelationId() {
                return correlationId;
            }

            @Override
            public String getBusinessCorrelationId() {
                return businessCorrelationId;
            }

            @Override
            public String getMessageId() {
                return messageId;
            }

            @Override
            public String toString() {
                return "Message [id=" + id + ", businessCorrelationId=" + businessCorrelationId + ", fromParticipantId= "
                        + fromParticipantId + ", toParticipantId=" + toParticipantId + ", replyToQueue=" + replyToQueue
                        + ", replyToQueueManager=" + replyToQueueManager + ", correlationId=" + correlationId
                        + ", messageId=" + messageId + ", payload=" + payload + ", expiration=" + expiration + "]";
            }
        }
    }
}
