package com.project.core.common.message;

import static java.util.Objects.requireNonNull;

public final class ParticipantMessage implements Message {

    private String id;
    private Object payload;
    private String fromParticipantId;
    private String toParticipantId;
    private String replyToQueue;
    private String replyToQueueManager;
    private String correlationId;
    private String businessCorrelationId;
    private String messageId;

    public ParticipantMessage(final String id, final Object payload) {
        this.id = requireNonNull(id);
        this.payload = requireNonNull(payload);
    }

    public ParticipantMessage(final String id, final String fromParticipantId, final String toParticipantId,
                              final String replyToQueue, final String replyToQueueManager, final String correlationId,
                              final String messageId, final String payload) {
        this(id, payload);
        this.fromParticipantId = fromParticipantId;
        this.toParticipantId = toParticipantId;
        this.replyToQueue = replyToQueue;
        this.replyToQueueManager = replyToQueueManager;
        this.correlationId = correlationId;
        this.messageId = messageId;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    @Override
    public String getFromParticipantId() {
        return fromParticipantId;
    }

    public void setFromParticipantId(String fromParticipantId) {
        this.fromParticipantId = fromParticipantId;
    }

    @Override
    public String getToParticipantId() {
        return toParticipantId;
    }

    public void setToParticipantId(String toParticipantId) {
        this.toParticipantId = toParticipantId;
    }

    @Override
    public String getReplyToQueue() {
        return replyToQueue;
    }

    public void setReplyToQueue(String replyToQueue) {
        this.replyToQueue = replyToQueue;
    }

    @Override
    public String getReplyToQueueManager() {
        return replyToQueueManager;
    }

    public void setReplyToQueueManager(String replyToQueueManager) {
        this.replyToQueueManager = replyToQueueManager;
    }

    @Override
    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    @Override
    public String getBusinessCorrelationId() {
        return businessCorrelationId;
    }

    public void setBusinessCorrelationId(String businessCorrelationId) {
        this.businessCorrelationId = businessCorrelationId;
    }

    @Override
    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @Override
    public String toString() {
        return "Message [id=" + id + ", businessCorrelationId=" + businessCorrelationId + ", fromParticipantId= "
                + fromParticipantId + ", toParticipantId=" + toParticipantId + ", replyToQueue=" + replyToQueue
                + ", replyToQueueManager=" + replyToQueueManager + ", correlationId=" + correlationId
                + ", messageId=" + messageId + ", payload=" + payload  + "]";
    }
}
