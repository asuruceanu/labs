package com.project.core.services.validation;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The MessageComparator Class.
 *
 * @author asuruceanu
 */
@Component
public class MessageComparator implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageComparator.class);

    /**
     * Message at hover over unmatched attribute
     */
    private static final String UNMATCHED_LINES_PRE_TAG_ATTRIBUTES = "class='unMatchedLine' title=\"Actual message attribute doesn't match the expected message attribute\"";

    /**
     * Message at hover over ignored tags
     */
    private static final String IGNORED_LINES_PRE_TAG_ATTRIBUTES = "class='ignoredLine' title='Line ignored while comparing the expected and actual message'";

    /**
     * The JSON tags to be ignored when comparing
     */
    private static String[] jsonTagsToBeIgnored = {};

    /**
     * The XML tags to be ignored when comparing
     */
    private static String xmlTagsToBeIgnored = null;

    /**
     * The Text tags to be ignored when comparing
     */
    private static String[] textTagsToBeIgnored = {};

    /**
     * List of lines in the expected message
     */
    private List<String> expectedMessageLines;

    /**
     * List of lines in the actual message
     */
    private List<String> actualMessageLines;
    private List<Delta> deltas;

    /**
     * The message format
     */
    private String messageFormat;

    /**
     * Sets for tags to be ignored for JSON message
     *
     * @param jsonTagsToBeIgnored
     */
    public static void setJsonTagsToBeIgnored(String[] jsonTagsToBeIgnored) {
        MessageComparator.jsonTagsToBeIgnored = jsonTagsToBeIgnored;
    }

    public static String[] getTextTagsToBeIgnored() {
        return textTagsToBeIgnored;
    }

    public static void setTextTagsToBeIgnored(String[] textTagsToBeIgnored) {
        MessageComparator.textTagsToBeIgnored = textTagsToBeIgnored;
    }

    /**
     * Gets the JSON tags to be ignored
     *
     * @return
     */
    public String[] getJsonTagsToBeIgnored() {
        return jsonTagsToBeIgnored;
    }

    /**
     * Sets the JSON tags to be ignored
     *
     * @param tagsToBeIgnored
     */
    public void setJsonTagsToBeIgnored(final String tagsToBeIgnored) {
        String[] arrTagsToBeIgnored;
        if (tagsToBeIgnored != null) {
            arrTagsToBeIgnored = tagsToBeIgnored.split(",");
        } else {
            arrTagsToBeIgnored = new String[0];
        }
        MessageComparator.setJsonTagsToBeIgnored(arrTagsToBeIgnored);


    }

    /**
     * Gets the XML tags to be ignored
     *
     * @return
     */
    public String getXmlTagsToBeIgnored() {
        return xmlTagsToBeIgnored;
    }

    /**
     * Sets the XML tags to be ignored
     *
     * @param xmlTagsToBeIgnored
     */
    public static void setXmlTagsToBeIgnored(String xmlTagsToBeIgnored) {
        MessageComparator.xmlTagsToBeIgnored = xmlTagsToBeIgnored;
    }

    /**
     * Initialization of variables
     *
     * @param expectedMessage
     * @param actualMessage
     * @param messageFormat
     */
    private void init(final String expectedMessage, final String actualMessage, final String messageFormat) {

        this.actualMessageLines = stringToLines(actualMessage);
        this.expectedMessageLines = stringToLines(expectedMessage);
        this.messageFormat = messageFormat;

        Patch patch = DiffUtils.diff(expectedMessageLines, actualMessageLines);
        this.deltas = patch.getDeltas();
    }

    /**
     * Builds the HTML output for comparing two XML files and save Result to File
     *
     * @param leftXMLMessage
     * @param rightXMLMessage
     * @param path
     */
    public File saveHTMLComparisonViewForXML(final String leftXMLMessage, final String rightXMLMessage, final String path) {

        String htmlComparisonView = getHTMLComparisonViewForXMLMessages(leftXMLMessage, rightXMLMessage);
        String timestampUUID;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmsssss");
        timestampUUID = sdf.format(new Date());

        saveMessage(path + File.separator + "expected_XML_" + timestampUUID + ".xml", leftXMLMessage);
        saveMessage(path + File.separator + "actual_XML_" + timestampUUID + ".xml", rightXMLMessage);
        return saveHTMLView(path + File.separator + "diff_XML_" + timestampUUID + ".html", htmlComparisonView);
    }

    /**
     * Builds the HTML output for comparing two JSON files and save Result to File
     *
     * @param leftJSONMessage
     * @param rightJSONMessage
     * @param path
     */
    public File saveHTMLComparisonViewForJSON(final String leftJSONMessage, final String rightJSONMessage, final String path) {

        String htmlComparisonView = getHTMLComparisonViewForJsonMessages(leftJSONMessage, rightJSONMessage);
        String timestampUUID;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmsssss");
        timestampUUID = sdf.format(new Date());
        saveMessage(path + File.separator + "expected_JSON_" + timestampUUID + ".json", leftJSONMessage);
        saveMessage(path + File.separator + "actual_JSON_" + timestampUUID + ".json", rightJSONMessage);
        return saveHTMLView(path + File.separator + "diff_JSON_" + timestampUUID + ".html", htmlComparisonView);
    }

    /**
     * Builds the HTML output for comparing two JSON files and save Result to File
     *
     * @param leftTextMessage
     * @param righTextMessage
     * @param path
     */
    public File saveHTMLComparisonViewForText(final String leftTextMessage, final String righTextMessage, final String path) {

        String htmlComparisonView = getHTMLComparisonViewForTextMessages(leftTextMessage, righTextMessage);
        String randomUUID;
        randomUUID = UUID.randomUUID().toString();
        saveMessage(path + File.separator + "expected_TXT_" + randomUUID + ".txt", leftTextMessage);
        saveMessage(path + File.separator + "actual_TXT_" + randomUUID + ".txt", righTextMessage);
        return saveHTMLView(path + File.separator + "diff_TXT_" + randomUUID + ".html", htmlComparisonView);
    }

    /**
     * Builds the HTML output for comparing two XML files
     *
     * @param leftXMLMessage
     * @param rightXMLMessage
     * @return
     */
    public String getHTMLComparisonViewForXMLMessages(final String leftXMLMessage, final String rightXMLMessage) {
        return getHTMLComparisonView(leftXMLMessage, rightXMLMessage, MessageFormat.XML.getValue());
    }

    /**
     * Save the XML or JSON Message to file
     *
     * @param path
     * @param stringToSave
     */
    private void saveMessage(String path, String stringToSave) {
        try {

            File file = new File(path);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(stringToSave);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Save the HTML Comparison View to file
     *
     * @param path
     * @param stringToSave
     */
    private File saveHTMLView(String path, String stringToSave) {
        String htmlStart = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<style>\n" +
                ".compareTable {\n" +
                "\ttable-layout: fixed;\n" +
                "}\n" +
                ".compareTable th {\n" +
                "\tword-wrap: break-word;\n" +
                "\tmax-width: 50%;\n" +
                "}\n" +
                ".compareTable tr td {\t\n" +
                "\tword-wrap: break-word;\n" +
                "\tborder: 1px solid black;\n" +
                "}\n" +
                "\n" +
                "table.compareTable {\n" +
                "\twidth: 100%;\n" +
                "\tborder-width: 1px;\n" +
                "\tborder-color: #666666;\n" +
                "\tborder-collapse: collapse;\n" +
                "\tborder-spacing:0px;'\n" +
                "}\n" +
                "\n" +
                ".compareTable td {\n" +
                "\tvertical-align: top;\n" +
                "\tborder-width: 1px 3px 1px 1px;\n" +
                "\tpadding: 0px;\n" +
                "\tborder-style: solid;\n" +
                "\tborder-color: #666666;\n" +
                "\tbackground-color: #ffffff;\n" +
                "\tfont: 11px/11px tahoma, arial, verdana, sans-serif;\n" +
                "}\n" +
                "\n" +
                ".compareTable th {\n" +
                "\twidth: 50%;\n" +
                "\tfont-size: 15px;\n" +
                "\tfont-weight: bold;\n" +
                "\tcolor: #04408C;\n" +
                "\tborder-width: 1px 3px 1px 1px;\n" +
                "\tpadding: 3px;\n" +
                "\tborder-style: solid;\n" +
                "\tborder-color: #666666;\n" +
                "\tbackground-image: url('../img/column-header-over-bg.gif');\n" +
                "}\n" +
                "\n" +
                ".compareTable pre {\n" +
                "background:rgb(51, 255, 173);\n" +
                "font-weight:bold;\n" +
                "\tmargin: 0;\n" +
                "\tfont-size: 12px;\n" +
                "\twhite-space: -moz-pre-wrap;\n" +
                "white-space: -o-pre-wrap;\n" +
                "word-wrap: break-word;\n" +
                "white-space: pre-wrap;\n" +
                "}\n" +
                "\n" +
                "pre.xmlMessage {\n" +
                "\n" +
                "\tmargin: 0;\n" +
                "\ttext-align: left;\n" +
                "\tfont-size: 15px;\n" +
                "}\n" +
                "\n" +
                "pre.message {\t\n" +
                "\tmargin: 0;\n" +
                "\ttext-align: left;\n" +
                "\tfont-size: 15px;\n" +
                "}\n" +
                "\n" +
                ".ignoredLine {\n" +
                "\tbackground: rgb(244, 223, 79) !important; /* Fall-back for browsers that don't support rgba */\n" +
                "}\n" +
                "\n" +
                ".unMatchedLine {\n" +
                "\tbackground: rgb(239, 119, 116) !important;\n" +
                "}\n" +
                "\n" +
                "\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>";
        String htmlEnd = "</body>\n" +
                "</html>";
        File file = null;

        try {

            file = new File(path);

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(htmlStart);
            bw.append(stringToSave);
            bw.append(htmlEnd);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    /**
     * Tests if expectedMessage and actualMessage are identical Text files
     *
     * @param expectedMessage
     * @param actualMessage
     * @return
     */
    public boolean areTextMessagesIdentical(final String expectedMessage, final String actualMessage) {
        return areMessagesIdentical(expectedMessage, actualMessage, MessageFormat.TEXT.getValue());
    }

    /**
     * Tests if expectedMessage and actualMessage are identical XML files
     *
     * @param expectedMessage
     * @param actualMessage
     * @return
     */
    public boolean areXMLMessagesIdentical(final String expectedMessage, final String actualMessage) {
        return areMessagesIdentical(expectedMessage, actualMessage, MessageFormat.XML.getValue());
    }

    /**
     * Builds the HTML output for comparing two Text files
     *
     * @param leftTextMessage
     * @param rightTextMessage
     * @return
     */
    public String getHTMLComparisonViewForTextMessages(final String leftTextMessage, final String rightTextMessage) {
        return getHTMLComparisonView(leftTextMessage, rightTextMessage, MessageFormat.TEXT.getValue());
    }

    /**
     * Builds the HTML output for comparing two JSON files
     *
     * @param leftJsonMessage
     * @param rightJsonMessage
     * @return
     */
    public String getHTMLComparisonViewForJsonMessages(final String leftJsonMessage, final String rightJsonMessage) {
        return getHTMLComparisonView(leftJsonMessage, rightJsonMessage, MessageFormat.JSON.getValue());
    }

    /**
     * Tests if expectedMessage and actualMessage are identical JSON files
     *
     * @param expectedMessage
     * @param actualMessage
     * @return
     */
    public boolean areJSONMessagesIdentical(final String expectedMessage, final String actualMessage) {
        return areMessagesIdentical(expectedMessage, actualMessage, MessageFormat.JSON.getValue());
    }

    private void fillHTMLForDeltaLines(final List<?> deltaLines, final StringBuilder html) {
        boolean highlighting = true;
        html.append("<pre ").append(UNMATCHED_LINES_PRE_TAG_ATTRIBUTES).append(">");

        for (Object o : deltaLines) {
            String line = o.toString().trim();
            if (ignoreLine(line)) {
                if (highlighting) {
                    html.append("</pre><pre ").append(IGNORED_LINES_PRE_TAG_ATTRIBUTES).append(">");
                    highlighting = false;
                }
            } else {
                if (!highlighting) {
                    html.append("</pre><pre ").append(UNMATCHED_LINES_PRE_TAG_ATTRIBUTES).append(">");
                }
            }

            if (o.toString() != null && o.toString().trim().length() > 0) {
                html.append("\n").append(StringEscapeUtils.escapeHtml(o.toString()));
            } else {
                html.append("\n&nbsp;");
            }
        }
        html.append("</pre>");
    }

    /**
     * Builds the HTML output for comparing two XML or JSON files
     *
     * @param expectedMessage
     * @param actualMessage
     * @param messageFormat   : XML or JSON
     * @return
     */
    private String getHTMLComparisonView(final String expectedMessage, final String actualMessage, final String messageFormat) {
        StringBuilder html = new StringBuilder();
        init(expectedMessage, actualMessage, messageFormat);

        StringBuilder leftMessage = new StringBuilder();
        StringBuilder rightMessage = new StringBuilder();
        int last = 0;

        for (Delta delta : deltas) {
            if (last + 1 < delta.getOriginal().getPosition()) {
                leftMessage.append("<pre>");
                rightMessage.append("<pre>");
                for (int i = last + 1; i < delta.getOriginal().getPosition(); i++) {
                    leftMessage.append("\n").append(StringEscapeUtils.escapeHtml(expectedMessageLines.get(i)));

                    String escapedActualLine = "";
                    //Take into account the case when the actual message is shorter than expected message
                    if (i < actualMessageLines.size()) {
                        escapedActualLine = StringEscapeUtils.escapeHtml(actualMessageLines.get(i));
                    }
                    rightMessage.append("\n").append(escapedActualLine);
                }
                leftMessage.append("</pre>");
                rightMessage.append("</pre>");
            }

            fillHTMLForDeltaLines(delta.getOriginal().getLines(), leftMessage);
            fillHTMLForDeltaLines(delta.getRevised().getLines(), rightMessage);

            //Adding empty lines when the number of lines is not equal in actual and expected message
            if (delta.getOriginal().getLines().size() > delta.getRevised().getLines().size()) {
                int numOfEmptyLines = delta.getOriginal().getLines().size() - delta.getRevised().getLines().size();
                rightMessage.append("<pre ").append(UNMATCHED_LINES_PRE_TAG_ATTRIBUTES).append(">");
                for (int i = 0; i < numOfEmptyLines; i++) {
                    rightMessage.append("\n&nbsp;");
                }
                rightMessage.append("</pre>");
            } else if (delta.getOriginal().getLines().size() < delta.getRevised().getLines().size()) {
                int numOfEmptyLines = delta.getRevised().getLines().size() - delta.getOriginal().getLines().size();
                leftMessage.append("<pre ").append(UNMATCHED_LINES_PRE_TAG_ATTRIBUTES).append(">");
                for (int i = 0; i < numOfEmptyLines; i++) {
                    leftMessage.append("\n&nbsp;");
                }
                leftMessage.append("</pre>");
            }

            last = delta.getOriginal().last();
        }

        //last is not delta
        if (last + 1 < expectedMessageLines.size()) {
            leftMessage.append("<pre>");
            rightMessage.append("<pre>");
            for (int i = last + 1; i < expectedMessageLines.size(); i++) {
                leftMessage.append("\n").append(StringEscapeUtils.escapeHtml(expectedMessageLines.get(i)));
                rightMessage.append("\n").append(StringEscapeUtils.escapeHtml(expectedMessageLines.get(i)));
            }
            leftMessage.append("</pre>");
            rightMessage.append("</pre>");
        }

        html.append("<table class='compareTable'>");
        html.append("<thead><tr><th>Expected Result</th><th>Actual Result</th></tr></thead>");
        html.append("<tbody><tr><td>");
        html.append(leftMessage.toString());
        html.append("</td><td>");
        html.append(rightMessage.toString());
        html.append("</td></tr></tbody></table>");

        return html.toString();
    }

    /**
     * Tests if two messages are identical
     *
     * @param expectedMessage the String value
     * @param actualMessage   the String value
     * @param messageFormat   : XML or JSON
     * @return true if the String values are equal, otherwise false
     */
    private boolean areMessagesIdentical(final String expectedMessage, final String actualMessage, final String messageFormat) {

        init(expectedMessage, actualMessage, messageFormat);

        for (Delta delta : deltas) {
            List<?> expectedLines = delta.getOriginal().getLines();
            for (Object o : expectedLines) {
                String line = o.toString().trim();
                if (!ignoreLine(line)) {
                    LOGGER.info("areMessagesIdentical found difference (expectedLines):" + line);
                    return false;
                }
            }
            List<?> actualLines = delta.getRevised().getLines();
            for (Object o : actualLines) {
                String line = o.toString().trim();
                if (!ignoreLine(line)) {
                    LOGGER.info("areMessagesIdentical found difference (actualLines):" + line);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Checks if a line should be ignored
     *
     * @param line
     * @return
     */
    private boolean ignoreLine(final String line) {

        if (MessageFormat.XML.getValue().equalsIgnoreCase(this.messageFormat)) {
            return ignoreXmlLine(line);
        } else if (MessageFormat.JSON.getValue().equalsIgnoreCase(this.messageFormat)) {
            return ignoreJsonLine(line);
        } else if(MessageFormat.TEXT.getValue().equalsIgnoreCase(this.messageFormat)) {
            return ignoreTextLine(line);
        }
        else {
            return false;
        }
    }

    /**
     * Checks if a line in XML should be ignored
     *
     * @param line
     * @return
     */
    private boolean ignoreXmlLine(final String line) {

        String tagName = getXMLTagName(line);

        if (Optional.ofNullable(getXmlTagsToBeIgnored()).isPresent()) {
            if (tagName != null && getXmlTagsToBeIgnored().matches(".*," + tagName + ",.*")) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get XML tag for a line
     *
     * @param line
     * @return
     */
    private String getXMLTagName(String line) {

        String tagName = null;
        line = line.trim();

        if (line.contains("<") && line.contains(">")) {
            // e.g. head:CreDt xmlns:head="urn:iso:std:iso:20022:tech:xsd:head.001.001.01"
            tagName = line.substring(1, line.indexOf(">"));
            // e.g. head:CreDt
            if (tagName.contains(" ")) {
                tagName = tagName.substring(0, tagName.indexOf(" "));
            }
            if (tagName.endsWith("/")) {
                tagName = tagName.substring(0, tagName.length() - 1);
            }
        }
        return tagName;
    }

    /**
     * Checks if a line in JSON should be ignored
     *
     * @param line
     * @return
     */
    private boolean ignoreJsonLine(final String line) {

        for (String s : getJsonTagsToBeIgnored()) {
            if (line.startsWith("\"" + s + "\"")) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a line in TEXT should be ignored
     *
     * @param line
     * @return
     */
    private boolean ignoreTextLine(final String line) {

        for (String s : getTextTagsToBeIgnored()) {
            if (line.trim().startsWith(s.trim())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Splits a string given as parameter and creates a list with the results found
     *
     * @param string
     * @return
     */
    private List<String> stringToLines(final String string) {

        String[] linesArray = string.split("\\r?\\n");
        List<String> linesList = new ArrayList<String>();
        Collections.addAll(linesList, linesArray);
        return linesList;
    }

    /**
     * Sets the XML tags to be ignored
     *
     * @param tagsToIgnore
     */
    public void setXMLTagsToBeIgnored(String tagsToIgnore) {

        if (tagsToIgnore != null) {
            tagsToIgnore = tagsToIgnore.replaceAll("\\s+", "");
            if (!tagsToIgnore.startsWith(",")) {
                tagsToIgnore = "," + tagsToIgnore;
            }
            if (!tagsToIgnore.endsWith(",")) {
                tagsToIgnore += ",";
            }
        }

        MessageComparator.setXmlTagsToBeIgnored(tagsToIgnore);
    }
}
