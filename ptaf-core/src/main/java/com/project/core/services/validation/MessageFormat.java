package com.project.core.services.validation;


import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * The MessageType enum.
 *
 * @author asuruceanu
 */
public enum MessageFormat {

    XML("XML"),
    JSON("JSON"),
    TEXT("TEXT");

    public static final Map<String, MessageFormat> LOOKUP = new HashMap<>();

    static {
        for (final MessageFormat a : EnumSet.allOf(MessageFormat.class)) {
            MessageFormat.LOOKUP.put(a.getValue(), a);
        }
    }

    private final String value;

    MessageFormat(String value) {
        this.value = value;
    }

    public static MessageFormat get(final String value) {
        MessageFormat messageFormat = MessageFormat.LOOKUP.get(value);
        if (messageFormat == null) {
            throw new IllegalArgumentException("An invalid message format value was passed in:" + value);
        }
        return messageFormat;
    }

    public String getValue() {
        return this.value;
    }
}
