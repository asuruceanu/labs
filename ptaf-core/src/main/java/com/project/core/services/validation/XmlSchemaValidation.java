package com.project.core.services.validation;

import com.project.core.common.message.Message;
import com.project.core.common.message.ParticipantMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.integration.xml.DefaultXmlPayloadConverter;
import org.springframework.integration.xml.XmlPayloadConverter;
import org.springframework.messaging.MessagingException;
import org.springframework.xml.validation.XmlValidator;
import org.springframework.xml.validation.XmlValidatorFactory;
import org.xml.sax.SAXParseException;

import java.io.IOException;
import java.util.UUID;

/**
 * @author Andrei.Suruceanu
 */
public class XmlSchemaValidation {

    private final Logger LOGGER = LoggerFactory.getLogger(XmlSchemaValidation.class);

    /**
     * The XML Validator
     */
    private XmlValidator xmlValidator;

    /**
     * The Payload converter
     */
    private XmlPayloadConverter xmlPayloadConverter = new DefaultXmlPayloadConverter();

    /**
     * Default constructor using fields
     *
     * @param schemaResource the resource
     * @throws IOException
     */
    public XmlSchemaValidation(Resource schemaResource) throws IOException {
        this(schemaResource, XmlValidatorFactory.SCHEMA_W3C_XML);
    }

    /**
     * Constructor.
     *
     * @param schemaResource the resource
     * @param schemaLanguage the schema language (e.g. xml-schema)
     * @throws IOException
     */
    private XmlSchemaValidation(Resource schemaResource, String schemaLanguage) throws IOException {
        if (schemaLanguage.contains(XmlValidatorFactory.SCHEMA_W3C_XML)) {
            this.xmlValidator = XmlValidatorFactory.createValidator(schemaResource, XmlValidatorFactory.SCHEMA_W3C_XML);
        } else {
            this.xmlValidator = XmlValidatorFactory.createValidator(schemaResource, XmlValidatorFactory.SCHEMA_RELAX_NG);
        }
    }

    /**
     * Validates the string representation of message payload
     *
     * @param xmlPayload the string representation of message payload
     */
    public void validateSchema(final String xmlPayload) {
        Message message = new ParticipantMessage(UUID.randomUUID().toString(), xmlPayload);
        validateSchema(message);
    }

    /**
     * Validates the payload of the message
     *
     * @param message the Message
     */
    public void validateSchema(final Message message) {

        try {
            SAXParseException[] exceptions = xmlValidator.validate(xmlPayloadConverter.convertToSource(message.getPayload()));

            if (exceptions.length > 0) {
                StringBuilder msg = new StringBuilder("Invalid XML message:\n");

                for (SAXParseException e : exceptions) {
                    msg.append("\t").append(e.getMessage());
                    msg.append(" (line=").append(e.getLineNumber());
                    msg.append(", col=").append(e.getColumnNumber()).append(")\n");
                }
                LOGGER.warn(msg.toString());
                throw new XmlSchemaValidationException("XSD schema validation failed", exceptions[0]);
            }

        } catch (IOException | XmlSchemaValidationException ioE) {
            throw new MessagingException("Exception applying schema validation", ioE);
        }
    }
}


