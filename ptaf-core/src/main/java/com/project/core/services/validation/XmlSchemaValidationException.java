package com.project.core.services.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.MessagingException;

/**
 * @author Andrei.Suruceanu
 */
public class XmlSchemaValidationException extends MessagingException {

    /**
     * The Logger
     */
    private final Logger LOGGER = LoggerFactory.getLogger(XmlSchemaValidationException.class);

    private static final long serialVersionID = 1L;

    XmlSchemaValidationException(final String errorMessage, final Throwable cause) {
        super(errorMessage);
        LOGGER.error("Schema validation error", cause);
    }
}
