package com.project.alm.plugin.client;


import com.project.alm.plugin.configurantion.AlmProperties;
import com.project.alm.plugin.configurantion.AlmPropertiesInitialisationException;

/**
 * @author Dumitru.Alexei
 */

public class ALMClientInstance {

    private static ALMClientInstance instance = null;

    private AlmClient almClient;

    private ALMClientInstance() throws AlmPropertiesInitialisationException {
        AlmProperties.getInstance().loadProperties("alm.properties");
        almClient = new AlmClientBuilder()
                .withHostName(AlmProperties.getInstance().getAlmHostName())
                .withPort(AlmProperties.getInstance().getAlmHostPort())
                .withUsername(AlmProperties.getInstance().getAlmUserName())
                .withPassword(AlmProperties.getInstance().getAlmUserPassword())
                .withProjectName(AlmProperties.getInstance().getAlmProjectName())
                .withDomainname(AlmProperties.getInstance().getAlmDomainName())
                .withOutputZipFileName(AlmProperties.getInstance().getAlmOutoutZipFileName())
                .withTestSetFolderName(AlmProperties.getInstance().getAlmTestSetFolderName())
                .withVersioned(AlmProperties.getInstance().getAlmVersioned())
                .build();
    }

    public static synchronized ALMClientInstance getInstance() throws AlmPropertiesInitialisationException {
        if (instance == null) {
            instance = new ALMClientInstance();
        }

        return instance;
    }

    public AlmClient getAlmClient() {
        return almClient;
    }
}
