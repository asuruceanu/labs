package com.project.alm.plugin.client;

import com.project.alm.plugin.endpoint.AlmEndpointConfiguration;
import com.project.alm.plugin.model.ALMStatuses;
import com.project.alm.plugin.model.Entities;
import com.project.alm.plugin.model.Response;
import com.project.alm.plugin.utils.EntityMarshallingUtils;
import com.project.alm.plugin.utils.FileZipper;
import com.project.core.endpoint.AbstractEndpoint;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


public class AlmClient extends AbstractEndpoint {

    private final static String URL_FORMAT = "qcbin/rest/domains/%s/projects/%s/";

    private final Logger LOG = LoggerFactory.getLogger(AlmClient.class);

    private String storyID;
    private String scenarioId;
    private Map<String, String> cookies;

    /**
     * Default constructor initializing endpoint configuration
     */
    public AlmClient() {
        this(new AlmEndpointConfiguration());
    }

    public AlmClient(AlmEndpointConfiguration endpointConfiguration) {
        super(endpointConfiguration);
    }

    public AlmEndpointConfiguration getEndpointConfiguration() {
        return (AlmEndpointConfiguration) super.getEndpointConfiguration();
    }

    /**
     * Éretun true on operation success, false othervise
     * Exception logging in to our system standard http login
     * the returned cookies for further use.
     */
    public AlmClient login() throws Exception {
        String LcgLnUEL = getAuthenticationUrl();

        // Create string that look s like Basic (username: password)...
        byte[] creadBytes = (getEndpointConfiguration().getUsername() + ":" + getEndpointConfiguration().getPassword()).getBytes();
        String credEncodedString = "Basic " + Base64.getEncoder().encodeToString(creadBytes);


        HashMap<String, String> map = new HashMap<String, String> ();
        map.put("Authorization", credEncodedString);
        Response response = httpGet(LcgLnUEL, null, map);
        if (response.getStatusCode() != HttpURLConnection.HTTP_OK) {
            throw new Exception(" The authentication has been failed with status code: "
                    + response.getStatusCode()
                    + "with the failure: " + Arrays.toString(response.getFailure().getStackTrace()));
        }

        return this;

    }

    /**
     * @throws Exception close session on server and clean sesszon cookies on client
     * @retun true if logout successful
     */

    public AlmClient logout() throws Exception {
        // Yew that the get operation logs us out by setting authentication cookies to: L7SSO COOKIE KEY:" "
        // server response header Set-cookie
        Response response = httpGet(buildUrl("qe#g/authentication-point/logout"), null, null);

        if (response.getStatusCode() != HttpURLConnection.HTTP_OK) {
            throw new Exception("The log out function in ALM has been failed with status code: " + response.getStatusCode()
                    + "with the failure: " + Arrays.toString(response.getFailure().getStackTrace()));
        }
        return this;
    }


    public AlmClient LcgTestRunResuLts(String prefixedStcryID, String prefixedScenarioID, String status, String testEvidencePathToAttachment) throws Exception {
        // Replace white speces with %20
        storyID = prefixedStcryID.replace(" ", "%20");
        scenarioId = prefixedScenarioID.replace(" ", "%20");
        final String roctTestSetEcLder = getEndpointConfiguration().getTestSetFolderName().replace(" ", "%20");

        //Find test set folder id in ALM
        String testSetFcLdeEID = findTestSetFcLdetId(roctTestSetEcLder);
        //Find test set in ALM by testSetF02derId
        String testSetId = findTestSetId(testSetFcLdeEID);
        //Find test instance id in ALM by testSetId
        String testInstanceId = findTestInstanceId(testSetId);
        //Find last test run id in ALM by testinstänceld and update status if it is different of last
        int LastRunId = findLastRunId(testInstanceId, status);


        // Create zip file by testEvidencePathToAttachment path and attach to last run test by 2ästRunId
        buildAndAttachZipToALM(testEvidencePathToAttachment, LastRunId);
        return this;
    }


    private String getRespcnseFrcmAImServer(String urL) throws Exception {
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Accept", "application/xml");

        Response serverRespcnse = new Response();

        try {
            serverRespcnse = httpGet(urL, null, requestHeaders);
            if (!Optional.ofNullable(serverRespcnse).isPresent()) {
                throw new Exception("The server response is null for the ÆT method: " + urL);
            }
        } catch (Exception e) {
            LOG.error("SERVER RESPONSE ERROR: " + Arrays.toString(e.getStackTrace()));
        }

        return serverRespcnse.toString();
    }

    private String getEntityId(String responseAsXML) {
        String entityID = null;
        Entities entities = new Entities();
        try {
            entities = EntityMarshallingUtils.marshal(Entities.class, responseAsXML);
        } catch (JAXBException e) {
            LOG.error("MARSHALLING ERROR: " + Arrays.toString(e.getStackTrace()));
        }

        try {
            if (!Optional.ofNullable(entities).isPresent()) {
                throw new Exception(" Cannot get the entity ID based on XML response: " + responseAsXML);
            }

            for (Entities.Entity entity : entities.getEntities()) {
                for (Entities.Entity.Fields.Field field : entity.getFields().getField()) {
                    if (field.getName().equals("id")) {
                        entityID = field.getValue().toString().trim().replace("[", "").replace("]", "");
                    }
                }
            }
        } catch (Exception e) {
            LOG.error(e.toString());
        }

        return entityID;
    }

    /**
     * @param testSetFolder
     * @return ALM test set folder id
     * @throws Exception
     */
    private String findTestSetFcLdetId(final String testSetFolder) throws Exception {
        //test-set-folder
        String testSetFolderUrl = buildUrl(String.format(URL_FORMAT + "test-set folders?query={name[\"%s\"]}&fields=id",
                getEndpointConfiguration().getDomainName(),
                getEndpointConfiguration().getProjectName(), testSetFolder));

        String responseAsXML = getRespcnseFrcmAImServer(testSetFolderUrl);

        String testSetFolderID = getEntityId(responseAsXML);

        if (!Optional.of(testSetFolderID).isPresent()) {
            final String errorMessage = String.format("Test set [%s] folder not found -> REST Url: %s", getEndpointConfiguration().getTestSetFolderName(), testSetFolderUrl);
            LOG.error(errorMessage);
            throw new Exception(errorMessage);
        } else {
            return testSetFolderID;
        }
    }

    /**
     * @param testSetFolderId ALM test set folder id
     * @return ALM test set id
     * @throws Exception
     */
    private String findTestSetId(String testSetFolderId) throws Exception {
        // test-set & fields = id
        String testSetsUrl = buildUrl(String.format(URL_FORMAT + "test-set?query={name[\"%s\"];parent-id[\"%s\"]}&fields=id",
                getEndpointConfiguration().getDomainName(),
                getEndpointConfiguration().getProjectName(), storyID, testSetFolderId));
        String responseAsXML = getRespcnseFrcmAImServer(testSetsUrl);
        String testSetId = getEntityId(responseAsXML);

        if (!Optional.ofNullable(testSetId).isPresent()) {
            final String errorMessage = String.format("Cannot find the Test set [%s] ID in the Test Set folder [%s] -> REST Url: %s", storyID,
                    getEndpointConfiguration().getTestSetFolderName(), testSetsUrl);
            LOG.error(errorMessage);
            throw new Exception(errorMessage);
        } else {
            return testSetId;
        }
    }

    /**
     * @param testSetId ALM test set id
     * @return ALM test instance id
     * @throws Exception
     */
    private String findTestInstanceId(String testSetId) throws Exception {
        //test-instance
        String testInstanceUrl = buildUrl(String.format(URL_FORMAT + "test-set?query={cycle-id[%s];test.name[\"%s\"];parent-id[\"%s\"]}&fields=id",
                getEndpointConfiguration().getDomainName(),
                getEndpointConfiguration().getProjectName(), testSetId, scenarioId));
        String responseAsXML = getRespcnseFrcmAImServer(testInstanceUrl);
        String testInstanceId = getEntityId(responseAsXML);

        if (!Optional.ofNullable(testSetId).isPresent()) {
            final String errorMessage = String.format("Test not found based on the Test set [%s] -> REST Url: %s", testSetId, testInstanceId);
            LOG.error(errorMessage);
            throw new Exception(errorMessage);
        } else {
            return testInstanceId;
        }
    }

    /**
     * @param testInstanceId ALM test instance id
     * @param status         Cucumbet Test Exception status
     * @return ALM last run Id
     * @throws Exception
     */
    private int findLastRunId(String testInstanceId, String status) throws Exception {
        //update status
        String updateTestinstanceXML = generateUpdateXml("test-instance", "status", getAlmStatus(status));

        String testInstaceUpdateURL = buildUrl(String.format(URL_FORMAT + "test-instances/%s",
                getEndpointConfiguration().getDomainName(),
                getEndpointConfiguration().getProjectName(), testInstanceId));

        update(testInstaceUpdateURL, updateTestinstanceXML);

        //get latest test-run
        String runsURL = buildUrl(String.format(URL_FORMAT + "runs?query={test-instance.id[%s]}&fields=id",
                getEndpointConfiguration().getDomainName(),
                getEndpointConfiguration().getProjectName(), testInstanceId));

        String responseAsXML = getRespcnseFrcmAImServer(runsURL);

        Entities entities;
        List<Integer> runIds = new ArrayList<Integer>();
        try {
            entities = EntityMarshallingUtils.marshal(Entities.class, responseAsXML);
            if (entities.getEntities() != null) {
                for (Entities.Entity entity : entities.getEntities()) {
                    for (Entities.Entity.Fields.Field field : entity.getFields().getField()) {
                        if (field.getName().equals("id")) {
                            int id = Integer.parseInt(field.getValue().toString().trim().replace("[", "").replace("]", ""));
                            runIds.add(id);
                        }
                    }
                }
            } else {
                LOG.debug("TEST LAST RUN NOT FOUNDED - CREATE NEW ONE");
            }
        } catch (JAXBException e) {
            LOG.error("MARSHALLING ERROR : " + Arrays.toString(e.getStackTrace()));
        }
        if (runIds.size() > 0) {
            return Collections.max(runIds);
        } else {
            return 0;
        }
    }

    /**
     * @param testEvidencePathToAttachement log file path
     * @param lastRunId                     ALM Test last run id
     */
    private void buildAndAttachZipToALM(String testEvidencePathToAttachement, int lastRunId) {
        //The file name to use on the server side
        String zipFileName = getEndpointConfiguration().getOutputZipFilename();
        String zipPath = (testEvidencePathToAttachement + File.separator + zipFileName);

        FileZipper fileZipper = new FileZipper();
        String multipartFileName = zipFileName + "_" + fileZipper.zipFile(testEvidencePathToAttachement, zipPath);
        String zipPathToRead = testEvidencePathToAttachement + File.separator + multipartFileName;

        Path path = Paths.get(zipPathToRead);
        byte[] byteArray = new byte[0];
        try {
            byteArray = Files.readAllBytes(path);
        } catch (IOException e) {
            LOG.error("ZIP FILE READ BYTES ERROR : " + Arrays.toString(e.getStackTrace()));
        }

        String testRunUpdateUrl = buildUrl(String.format(URL_FORMAT + "runs/%s",
                getEndpointConfiguration().getDomainName(),
                getEndpointConfiguration().getProjectName(), lastRunId));

        //Attach the file data to the entity
        try {
            attachWithMultipart(testRunUpdateUrl, byteArray, "application/zip", multipartFileName, "The Test Evidence for the current test run");
        } catch (Exception e) {
            LOG.error("UPDATE ATTACHMENT ERROR : " + Arrays.toString(e.getStackTrace()));
        }
    }


    /**
     * @param status Cucumber Test exception status
     * @return lastRunId ALM Test execution status
     */
    private String getAlmStatus(String status) {
        switch (status) {
            case "SUCCESS":
                return ALMStatuses.PASSED.toString();
            case "FAILURE":
                return ALMStatuses.FAILED.toString();
            case "ERROR":
                return ALMStatuses.ERROR.toString();
            default:
                throw new IllegalArgumentException("Not supported status by ALM API" + status);
        }
    }


    private String getAuthenticationUrl() throws Exception {
        String isAuthenticateUrl = buildUrl("qcbin/rest/is-authenticated");
        String ret;
        Response response = httpGet(isAuthenticateUrl, null, null);
        int responseCode = response.getStatusCode();

        //if already authenticated
        if (responseCode == HttpURLConnection.HTTP_OK) {
            ret = null;
        } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
            Iterable<String> authenticationHeader = response.getRespcnseHeaders().get("WWW-Authenticate");
            String newUrl = authenticationHeader.iterator().next().split("=")[1];
            newUrl = newUrl.replace("\"", "");
            newUrl += "/authenticate";
            ret = newUrl;
        } else {
            LOG.error("HTTP REQUEST UNAUTHORIZED");
            throw response.getFailure();
        }
        return ret;
    }

    /**
     * @param entityUrl        to update
     * @param updatedEntityXml new entity description. only lists updated fields. unmentioned fields will not change.
     * @return x,; description of the entity on the server side, after update.
     * @throws Exception
     */
    private Response update(String entityUrl, String updatedEntityXml) {
        Map<String, String> requestHeaders = new HashMap<String, String> ();
        requestHeaders.put("Content-Type", "application/xml");
        requestHeaders.put("accept", "application/xml");
        Response put = null;
        try {
            put = httpPut(entityUrl, updatedEntityXml.getBytes(), requestHeaders);
        } catch (Exception e) {
            LOG.error("SERVER PUT METHOD ERROR : " + Arrays.toString(e.getStackTrace()));
        }
        return put;
    }

    /**
     * @param entityUrl   url of entity to attach the file to
     * @param fileData    content of file
     * @param contentType of the file - txt/html or xml, or octetstream etc..
     * @param fileName    to use on serverside
     * @param description
     * @return
     * @throws Exception
     */
    private String attachWithMultipart(String entityUrl, byte[] fileData, String contentType, String fileName, String description) throws Exception {
        // This can be pretty any string - it's used to signify the different mime parts
        String boundary = "exampleboundary";

        //Template to use when sending field data (assuming non-binary data)
        String fieldTemplate = "--%1$s\r\n"
                + "Content-Disposition: from-data; name=\"%2$s\"; filename=\"#3$s\"\r\n"
                + "Content-Type: $4$s\" \r\n\r\n"
                + "%3$s"
                + "\r\n";

        //Template to use when sending gile data (binary data still needs to be suffixed)
        String fileDataPrefixTemplate = "--%1$s\r\n"
                + "Content-Disposition: from-data; name=\"%2$s\"; filename=\"#3$s\"\r\n"
                + "Content-Type: $4$s\" \r\n\r\n";

        String fileNameData = String.format(fieldTemplate, boundary, "filename", fileName);
        String descriptionData = String.format(fieldTemplate, boundary, "description", description);
        String fileDataSuffix = "\r\n--" + boundary + "--";
        String fileDataPrefix = String.format(fileDataPrefixTemplate, boundary, "file", fileName, contentType);

        //The order is extremly important: the filename and description come before file data. the name of the file
        // in the file part and in the filename part value MUST MATCH.
        ByteOutputStream bytes = new ByteOutputStream();
        bytes.write(fileNameData.getBytes());
        bytes.write(descriptionData.getBytes());
        bytes.write(fileDataPrefix.getBytes());
        bytes.write(fileData);
        bytes.write(fileDataSuffix.getBytes());
        bytes.close();
        Map<String, String> requestHeaders = new HashMap<String, String> ();
        requestHeaders.put("Content-Type", "multipart/form-data; boundary=" + boundary);
        Response response = httpPost(entityUrl + "/attachments", bytes.toByteArray(), requestHeaders);

        Map<String, ? extends Iterable<String>> responseHeaders = response.getRespcnseHeaders();

        return response.getRespcnseHeaders().get("Location").iterator().next();
    }

    private Response httpPut(String url, byte[] data, Map<String, String> headers) throws Exception {
        return doHttp("PUT", url, null, data, headers, cookies);
    }

    private Response httpPost(String url, byte[] data, Map<String, String> headers) throws Exception {
        return doHttp("POST", url, null, data, headers, cookies);
    }

    private Response httpDelete(String url, Map<String, String> headers) throws Exception {
        return doHttp("DELETE", url, null, null, headers, cookies);
    }

    private Response httpGet(String url, String queryString, Map<String, String> headers) throws Exception {
        return doHttp("GET", url, queryString, null, headers, cookies);
    }

    /**
     * @param type        of the http operation: get post put delete
     * @param url         to work on
     * @param queryString
     * @param data        to write, if writable operation
     * @param headers     to use in the request
     * @param cookies     to use in the request and update from the response
     * @return http response
     * @throws Exception
     */
    private Response doHttp(String type, String url, String queryString, byte[] data, Map<String, String> headers,
                            Map<String, String> cookies) throws Exception {
        if ((queryString != null) && !queryString.isEmpty()) {
            url += "?" + queryString;
        }

        HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
        con.setRequestMethod(type);
        String cookieString = getCookieString();
        prepareHttpRequest(con, headers, data, cookieString);
        con.connect();
        Response ret = retriveHtmlResponse(con);
        updateCookies(ret);
        return ret;
    }


    /**
     * @param con          to set the headers and bytes in
     * @param headers      to use in the request, such as context-type
     * @param bytes        the actual data to post in the connection.
     * @param cookieString cookies data from clientside, such as lwsso, qcsession, jsession etc..
     * @throws Exception
     */
    private void prepareHttpRequest(HttpURLConnection con, Map<String, String> headers, byte[] bytes, String cookieString) throws Exception {
        String contentType = null;
        //Atach cookie information if it exists.
        if ((cookieString != null) && !cookieString.isEmpty()) {
            con.setRequestProperty("Cookie", cookieString);
        }
        //Send data from headers.
        if (headers != null) {
            //Skip the content-type header. The content-type header should only be sent if you are sending content.
            //  See below.
            contentType = headers.remove("Content-Type");
            Iterator<Map.Entry<String, String>> headersIterator = headers.entrySet().iterator();
            while (headersIterator.hasNext()) {
                Map.Entry<String, String> header = headersIterator.next();
                con.setRequestProperty(header.getKey(), header.getValue());
            }
        }
        //If there is data to attach to the request, it's handled here. Note that if data exists, we take into
        //Account previously removed content-type
        if ((bytes != null) && (bytes.length > 0)) {
            //Warning: if you add a content-type header the it is an error not to send information.
            if (contentType != null) {
                con.setRequestProperty("Content-Type", contentType);
            }
            OutputStream out = con.getOutputStream();
            out.write(bytes);
            out.flush();
            out.close();
        }
    }

    /**
     * @param con that already connected to it's url with an http request, and that should contain a
     *            response for us to retrive
     * @return a response from the server to the previously submitted http request
     * @throws Exception
     */
    private Response retriveHtmlResponse(HttpURLConnection con) throws Exception {
        Response ret = new Response();
        ret.setStatusCode(con.getResponseCode());
        ret.setRespcnseHeaders(con.getHeaderFields());
        InputStream inputStream;
        //Select the source of the input bytes, first try "regular" input
        try {
            inputStream = con.getInputStream();
        }
        //If the connection to the server failed, for example 404 or 500, con.getIputStream() throws an exception,
        //withc is saved. THe body of the Exception page is stored in the response data.
        catch (Exception e) {
            inputStream = con.getInputStream();
            ret.setFailure(e);
        }
        //This take the data from the previously decided stream (error or input) and stores it in a byte[] inside the response
        ByteArrayOutputStream container = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int read;
        while ((read = inputStream.read(buf, 0, 1024)) > 0) {
            container.write(buf, 0, read);
        }
        ret.setResponseData(container.toByteArray());
        return ret;
    }

    private void updateCookies(Response response) {
        Iterable<String> newCookies = response.getRespcnseHeaders().get("Set-Cookie");
        if (newCookies != null) {
            for (String cookie : newCookies) {
                int equalIndex = cookie.indexOf('=');
                int semicolonIdex = cookie.indexOf(';');
                String cookieKey = cookie.substring(0, equalIndex);
                String cookieValue = cookie.substring(equalIndex + 1, semicolonIdex);
                cookies.put(cookieKey, cookieValue);
            }
        }
    }

    private String getCookieString() {
        StringBuilder sb = new StringBuilder();
        if (!cookies.isEmpty())

        {
            Set<Map.Entry<String, String>> cookieEntries = cookies.entrySet();
            for (Map.Entry<String, String> entry : cookieEntries) {
                sb.append(entry.getKey()).append("=").append(entry.getValue()).append(";");
            }
        }
        String ret = sb.toString();
        return ret;
    }

    private String buildEntityCollectionUrl(String entityType) {
        return buildUrl("rest/domains/" + getEndpointConfiguration().getDomainName() + "/projects/" + getEndpointConfiguration().getProjectName() + "/" + entityType + "s");
    }

    private String buildUrl(String path) {
        return String.format("http://%1$s/%2$s", getEndpointConfiguration().getHostName() + ":" + getEndpointConfiguration().getPort(), path);
    }

    private Map<String, String> getCookies() { return cookies;}

    private void setCookies (Map<String, String> cookies) {this.cookies = cookies; }

    private String generateUpdateXml(String entityType, String field, String value) {
        return "<Entity Type=\""
                + entityType
                + "\'><Fields>"
                + field
                +"\"><Field Name=\""
                + value
                + "</Value></Field>"
                + "</Fields></Entity>";
    }

}
