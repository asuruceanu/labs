package com.project.alm.plugin.client;

import com.project.core.endpoint.AbstractEndpointBuilder;

public class AlmClientBuilder extends AbstractEndpointBuilder<AlmClient> {

    private AlmClient endpoint = new AlmClient();

    @Override
    protected AlmClient getEndpoint() {
        return endpoint;
    }

    /**
     * Sets the hostName property.
     *
     * @param hostName the host name
     * @return the JmxClientBuilder
     */
    public AlmClientBuilder withHostName(String hostName) {
        endpoint.getEndpointConfiguration().setHostName(hostName);
        return this;
    }

    /**
     * Sets the prot property
     *
     * @param port the port
     * @return JmxClientBuilder
     */
    public AlmClientBuilder withPort(int port) {
        endpoint.getEndpointConfiguration().setPort(port);
        return this;
    }

    /**
     * Set the userName property
     *
     * @param username
     * @return
     */
    public AlmClientBuilder withUsername(String username) {
        endpoint.getEndpointConfiguration().setUsername(username);
        return this;
    }

    /**
     * Set the password property
     *
     * @param password
     * @return
     */
    public AlmClientBuilder withPassword(String password) {
        endpoint.getEndpointConfiguration().setPassword(password);
        return this;
    }


    /**
     * Set the domainName property
     *
     * @param domainname
     * @return
     */
    public AlmClientBuilder withDomainname(String domainname) {
        endpoint.getEndpointConfiguration().setDomainName(domainname);
        return this;
    }


    /**
     * Set the projectname property
     *
     * @param projectName
     * @return
     */
    public AlmClientBuilder withProjectName(String projectName) {
        endpoint.getEndpointConfiguration().setProjectName(projectName);
        return this;
    }

    /**
     * Set the TestSetFolderName property
     *
     * @param testSetFolderName
     * @return
     */
    public AlmClientBuilder withTestSetFolderName(String testSetFolderName) {
        endpoint.getEndpointConfiguration().setTestSetFolderName(testSetFolderName);
        return this;
    }

    /**
     * Set the output zip name property
     *
     * @param outputZipFileName
     * @return
     */
    public AlmClientBuilder withOutputZipFileName(String outputZipFileName) {
        endpoint.getEndpointConfiguration().setOutputZipFilename(outputZipFileName);
        return this;
    }

    /**
     * Set the version property
     *
     * @param versioned
     * @return
     */
    public AlmClientBuilder withVersioned(Boolean versioned) {
        endpoint.getEndpointConfiguration().setVersioned(versioned);
        return this;
    }
}
