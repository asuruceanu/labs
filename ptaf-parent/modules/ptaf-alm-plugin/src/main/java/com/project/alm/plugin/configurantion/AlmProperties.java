package com.project.alm.plugin.configurantion;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.eclipse.jetty.util.StringUtil.isBlank;


public class AlmProperties {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlmProperties.class);
    private static final String ALM_HOST_NAME = "alm.host.name";
    private static final String ALM_HOST_PORT = "alm.host.port";
    private static final String ALM_USER_PASSWORD = "alm.user.password";
    private static final String ALM_DOMAIN = "alm.domain";
    private static final String ALM_PROJECT = "alm.project";
    private static final String ALM_VERSIONED = "alm.versioned";
    private static final String ALM_TEST_SET_FOLDER_NAME = "alm.test.set.folder.name";
    private static final String ALM_OUTPUT_ZIP_FILE_NAME = "alm.output.zip.file.name";
    private static final String ALM_USER_USERNAME = "alm.user.username";

    private static AlmProperties instance = null;

    private String almHostName;
    private int almHostPort;
    private String almUserName;
    private String almUserPassword;
    private String almDomainName;
    private String almProjectName;
    private Boolean almVersioned;
    private String almTestSetFolderName;
    private String almOutoutZipFileName;

    private AlmProperties() {
    }

    public static synchronized AlmProperties getInstance() {
        if (instance == null) {
            instance = new AlmProperties();
        }

        return instance;
    }

    public static String getPropertyValue(Configuration configuration, String paramKey) throws AlmPropertiesInitialisationException {
        String paramValue = configuration.getString(paramKey);
        if (isBlank(paramValue)) {
            LOGGER.error(paramKey + " property is not set");
            throw new AlmPropertiesInitialisationException(paramKey + " property is not set");
        } else {
            return paramValue.trim();
        }
    }

    public void loadProperties(String filename) throws AlmPropertiesInitialisationException {
        try {
            Configuration ce = this.getPropertiesConfig(filename);
            getInstance().setAlmUserName(getPropertyValue(ce, ALM_USER_USERNAME));
            getInstance().setAlmHostName(getPropertyValue(ce, ALM_HOST_NAME));
            getInstance().setAlmUserPassword(getPropertyValue(ce, ALM_USER_PASSWORD));
            getInstance().setAlmDomainName(getPropertyValue(ce, ALM_DOMAIN));
            getInstance().setAlmHostPort(Integer.valueOf(getPropertyValue(ce, ALM_HOST_PORT)));
            getInstance().setAlmProjectName(getPropertyValue(ce, ALM_PROJECT));
            getInstance().setAlmVersioned(Boolean.valueOf(getPropertyValue(ce, ALM_VERSIONED)));
            getInstance().setAlmTestSetFolderName(getPropertyValue(ce, ALM_TEST_SET_FOLDER_NAME));
            getInstance().setAlmOutoutZipFileName(getPropertyValue(ce, ALM_OUTPUT_ZIP_FILE_NAME));
        } catch (ConfigurationException var3) {
            LOGGER.error("Error while loading the ALM properties file:" + filename);
            throw new AlmPropertiesInitialisationException("Error while loading the ALM properties file");
        }
    }

    public Boolean getPropertyAsBoolean(Configuration configuration, String paramKey) throws AlmPropertiesInitialisationException {
        final String propertyValue = getPropertyValue(configuration, paramKey);

        if(propertyValue.equalsIgnoreCase("Y") || propertyValue.equalsIgnoreCase("YES")) {
            return true;
        } else if (propertyValue.equalsIgnoreCase("N") || propertyValue.equalsIgnoreCase("NO")){
            return false;
        }

        return Boolean.parseBoolean(propertyValue);
    }

    protected PropertiesConfiguration getPropertiesConfig(final String fileName) throws ConfigurationException {
        PropertiesConfiguration spCustomConfig = new PropertiesConfiguration();
        spCustomConfig.setDelimiterParsingDisabled(true);
        spCustomConfig.load(fileName);
        return spCustomConfig;
    }

    public String getAlmHostName() {
        return almHostName;
    }

    public void setAlmHostName(String almHostName) {
        this.almHostName = almHostName;
    }

    public int getAlmHostPort() {
        return almHostPort;
    }

    public void setAlmHostPort(int almHostPort) {
        this.almHostPort = almHostPort;
    }

    public String getAlmUserName() {
        return almUserName;
    }

    public void setAlmUserName(String almUserName) {
        this.almUserName = almUserName;
    }

    public String getAlmUserPassword() {
        return almUserPassword;
    }

    public void setAlmUserPassword(String almUserPassword) {
        this.almUserPassword = almUserPassword;
    }

    public String getAlmDomainName() {
        return almDomainName;
    }

    public void setAlmDomainName(String almDomainName) {
        this.almDomainName = almDomainName;
    }

    public String getAlmProjectName() {
        return almProjectName;
    }

    public void setAlmProjectName(String almProjectName) {
        this.almProjectName = almProjectName;
    }

    public Boolean getAlmVersioned() {
        return almVersioned;
    }

    public void setAlmVersioned(Boolean almVersioned) {
        this.almVersioned = almVersioned;
    }

    public String getAlmTestSetFolderName() {
        return almTestSetFolderName;
    }

    public void setAlmTestSetFolderName(String almTestSetFolderName) {
        this.almTestSetFolderName = almTestSetFolderName;
    }

    public String getAlmOutoutZipFileName() {
        return almOutoutZipFileName;
    }

    public void setAlmOutoutZipFileName(String almOutoutZipFileName) {
        this.almOutoutZipFileName = almOutoutZipFileName;
    }

}
