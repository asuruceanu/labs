package com.project.alm.plugin.configurantion;

public class AlmPropertiesInitialisationException extends Exception{

    private static final long serialVersionUID = 5783255493979848088L;

    public AlmPropertiesInitialisationException() {
    }

    public AlmPropertiesInitialisationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public AlmPropertiesInitialisationException(String message, Throwable cause) { super(message, cause); }

    public AlmPropertiesInitialisationException(String message) { super(message); }

    public AlmPropertiesInitialisationException(Throwable cause) { super(cause);}
}
