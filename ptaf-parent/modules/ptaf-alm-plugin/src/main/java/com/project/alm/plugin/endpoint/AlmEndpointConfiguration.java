package com.project.alm.plugin.endpoint;

import com.project.core.endpoint.AbstractEndpointConfiguration;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.HashMap;
import java.util.Map;

public class AlmEndpointConfiguration extends AbstractEndpointConfiguration implements ApplicationContextAware{

    private String hostName;

    private int port = 8080;

    private String domainName;

    private String projectName;

    private String testSetFolderName;

    private String outputZipFilename;

    private String username;

    private String password;

    private boolean isVersioned;

    private Map<String, Object> enviromentProperties = new HashMap<>();

    private ApplicationContext applicationContext;

    public String getDomainName() {return domainName;}

    public void setDomainName(String domainName) { this.domainName = domainName; }

    public String getProjectName() {return projectName;}

    public void setProjectName(String projectName) { this.projectName = projectName; }

    public int getPort() {return port; }

    public void setPort(int port) { this.port = port; }

    public String getTestSetFolderName() { return testSetFolderName; }

    public void setTestSetFolderName(String testSetFolderName) { this.testSetFolderName = testSetFolderName; }

    public String getHostName() {return hostName; }

    public void  setHostName(String hostName) {this.hostName = hostName; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public Map<String, Object> getEnviromentProperties() { return enviromentProperties; }

    public void setEnviromentProperties(Map<String, Object> enviromentProperties) {
        this.enviromentProperties = enviromentProperties;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public String getOutputZipFilename() { return outputZipFilename; }

    public void setOutputZipFilename(String outputZipFilename) { this.outputZipFilename = outputZipFilename; }

    public boolean isVersioned() { return isVersioned; }

    public void setVersioned(boolean versioned) { isVersioned = versioned; }
}
