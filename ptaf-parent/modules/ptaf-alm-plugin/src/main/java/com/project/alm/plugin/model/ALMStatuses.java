package com.project.alm.plugin.model;

public enum ALMStatuses {
    PASSED("Passed"),
    FAILED("Failed"),
    ERROR("Error"),
    No_RUN("No run");

    private final String almStatus;
    private ALMStatuses(final String almStatus) { this.almStatus = almStatus; }

    @Override
    public String toString() { return almStatus; }
}
