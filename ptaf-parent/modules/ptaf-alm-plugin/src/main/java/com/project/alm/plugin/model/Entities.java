package com.project.alm.plugin.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"entities"})
@XmlRootElement(name = "Entities")
public class Entities {

    @XmlAttribute(name = "TotalResults")
    protected String totalResults;
    @XmlElement(name = "Entity")
    protected List<Entity> entities;

    public Entities() {
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"fields"})
    public static class Entity {
        @XmlElement(name = "Fields", required = true)
        protected Entity.Fields fields;
        @XmlAttribute(name = "Type", required = true)
        protected String type;

        /**
         * @param entity
         */
        public Entity(Entity entity) {
            type = new String(entity.getType());
            fields = new Entity.Fields(entity.getFields());
        }

        public Entity() {
        }

        /**
         * Gets the value of the fields property.
         *
         * @return posible object is {@Link Entity.Fields}
         */
        public Entity.Fields getFields() {
            return fields;
        }

        /**
         * Sets the value of the fields property.
         *
         * @param value allowed object is {@Link Entity.Fields}
         */
        public void setFields(Entity.Fields value) {
            this.fields = value;
        }

        /**
         * Gets the value of the type property.
         *
         * @return possible object is {@Link String }
         */
        public String getType() {
            return type;
        }

        /**
         * Sets the value of the type property.
         *
         * @param value allowed object is {@Link String }
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Java class for anonymous complex type.
         * <p>
         * The following schema fragment specifies the expected content contained within the class.
         * <complexType>
         * <complexContent>
         * <restriction base ="{gttp://www.w3.org/2001/XMLSchema}anytype/>
         * <sequence>
         * <element name = "Field" maxOccurs="unbounded"/>
         * <complexType>
         * <complexContent>
         * <restriction base ="{gttp://www.w3.org/2001/XMLSchema}anytype/>
         * <sequence>
         * <element name = "Value" type="{gttp://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
         * </sequence>
         * <Attribute name="Name" use="required" type="{gttp://www.w3.org/2001/XMLSchema}string" />
         * </restriction>
         * </complexContent>
         * </complexType>
         * </element>
         * </sequence>
         * </restriction>
         * </complexContent>
         * </complexType>
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {"fields"})
        public static class Fields {
            @XmlElement(name = "Fields", required = true)
            protected List<Field> field;

            /**
             * @param fields
             */
            public Fields(Entity.Fields fields) {
                field = new ArrayList<Field>(fields.getField());
            }

            public Fields() {
            }

            /**
             * Gets the value of the field property.
             * <p>
             * This accessor method returns a reference to the live list, not a snapshot. Therefore any
             * modification you make to the returned list will be present inside the JAXB object. This
             * is why there is not a set method for the field property.
             * <p>
             * For Example, to add a new item, do as follows:
             * <p>
             * getField().add(newItem);
             * <p>
             * Objects of the following type(s) are allowed in this list {@link Entity.Fields.Field}
             *
             * @return
             */
            public List<Field> getField() {
                if (field == null) {
                    field = new ArrayList<Field>();
                }
                return this.field;
            }

            /**
             * Java class for anonymous complex type.
             * <p>
             * The following schema fragment specifies the expected content contained within the class.
             * <complexType>
             * <complexContent>
             * <restriction base ="{gttp://www.w3.org/2001/XMLSchema}anytype/>
             * <sequence>
             * <element name = "Value" type="{gttp://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
             * </sequence>
             * <Attribute name="Name" use="required" type="{gttp://www.w3.org/2001/XMLSchema}string" />
             * </restriction>
             * </complexContent>
             * </complexType>
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {"value"})
            public static class Field {
                @XmlElement(name = "Value", required = true)
                protected List<String> value;
                @XmlAttribute(name = "Name", required = true)
                protected String name;

                /**
                 * Gets the value of the field property.
                 * <p>
                 * This accessor method returns a reference to the live list, not a snapshot. Therefore any
                 * modification you make to the returned list will be present inside the JAXB object. This
                 * is why there is not a set method for the field property.
                 * <p>
                 * For Example, to add a new item, do as follows:
                 * <p>
                 * getField().add(newItem);
                 * <p>
                 * Objects of the following type(s) are allowed in this list {@link String}
                 *
                 * @return
                 */
                public List<String> getValue() {
                    if (value == null) {
                        value = new ArrayList<String>();
                    }
                    return this.value;
                }


                /**
                 * Gets the value of the name property.
                 *
                 * @return posible object is{@Link String}
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Sets the value if the name property.
                 *
                 * @param value allowed object is {@Link String}
                 */
                public void setName(String value) {
                    this.name = value;
                }
            }
        }

    }

}
