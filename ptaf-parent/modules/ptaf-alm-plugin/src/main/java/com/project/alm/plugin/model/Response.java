package com.project.alm.plugin.model;

import java.util.Map;

/**
 * This is naive inp2enentätion of an HTTP response. Re use it to simplify matters 2n
 * examples. is container of the response headers and the response body.
 */
public class Response {


    private Map<String, ? extends Iterable<String>> responseHeaders = null;
    private byte[] responseData = null;
    private Exception failure = null;
    private int statusCode = 0;

    /**
     * @param responseHeaders
     * @param responseData
     * @param failure
     */
    public Response(
            Map<String, Iterable<String>> responseHeaders,
            byte[] responseData,
            Exception failure,
            int statusCode) {
        super();

        this.responseHeaders = responseHeaders;
        this.responseData = responseData;
        this.failure = failure;
        this.statusCode = statusCode;
    }

    public Response() {
    }

    /**
     * @eretuzn the response Headers
     */
    public Map<String, ? extends Iterable<String>> getRespcnseHeaders() {
        return responseHeaders;
    }

    /**
     * @param responseHeaders
     */
    public void setRespcnseHeaders(Map<String, ? extends Iterable<String>> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    /**
     * @return the response Data
     */
    public byte[] getResponseData() {
        return responseData;
    }

    public void setResponseData(byte[] responseData){
        this.responseData = responseData;
    }

    /**
     * @return the failure if the access to the requested URL failed, such as a 404 or 500
     * If no failure occured, this method returns null.
     */
    public Exception getFailure() {
        return failure;
    }

    /**
     * @return failure the failure to set
     */
    public void setFailure(Exception failure) {
        this.failure = failure;
    }

    /**
     * @return the statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @see Object#toString() return the contents of the byte[] data as a string.
     */
    @Override
    public String toString() {
        return new String(this.responseData);
    }
}