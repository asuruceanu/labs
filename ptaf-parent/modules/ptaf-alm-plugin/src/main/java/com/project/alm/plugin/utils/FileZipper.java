package com.project.alm.plugin.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileZipper {
    private List<String> fileList;

    public FileZipper() {
        fileList = new ArrayList<String>();
    }

    /**
     * zip file
     *
     * @param sourceFolder  input source folder location
     * @param outputZipFile output ZIP file location
     */
    public String zipFile(String sourceFolder, String outputZipFile) {
        generateFileList(new File(sourceFolder), sourceFolder);

        /* log file name timestamp */
        String zipTimestamp;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmSS");
        zipTimestamp = sdf.format(new Date()).toString();
        String fullZipPatch = outputZipFile + "_" + zipTimestamp + ".zip";

        byte[] buffer = new byte[1024];

        try {
            FileOutputStream fos = new FileOutputStream(fullZipPatch);
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (String file : this.fileList) {
                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);

                FileInputStream in = new FileInputStream(sourceFolder + File.separator + file);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();

            }
            zos.closeEntry();
            zos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return zipTimestamp + ".zip";
    }

    /**
     * Traverse a directory and get all files,
     * and add the file into fileList
     *
     * @param node         file or directory
     * @param sourceFolder
     */
    private void generateFileList(File node, String sourceFolder) {
        //add file only
        if (node.isFile()) {
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString(), sourceFolder));
        }
        //iterate through directory
        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename), sourceFolder);
            }
        }
    }

    private String generateZipEntry(String file, String sourceFolder) {
        return file.substring(sourceFolder.length() + 1, file.length());
    }
}
