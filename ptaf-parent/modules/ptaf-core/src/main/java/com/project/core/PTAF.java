package com.project.core;

import java.nio.charset.Charset;

/**
 * @author Dumitru.Alexei
 */
public final class PTAF {

    /**
     * File encoding system property
     */
    public static final String PTAF_FILE_ENCODING_PROPERTY = "ptaf.file.encoding";
    public static final String PTAF_FILE_ENCODING = System.getProperty(PTAF_FILE_ENCODING_PROPERTY, Charset.defaultCharset().displayName());
}
