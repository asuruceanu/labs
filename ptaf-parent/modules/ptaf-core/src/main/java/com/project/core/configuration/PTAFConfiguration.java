package com.project.core.configuration;

import com.project.core.util.EnvironmentVariables;

public interface PTAFConfiguration {

    void setIfUdefined(String property, String value);

    PTAFConfiguration copy();

    PTAFConfiguration withEnviromentVariables(EnvironmentVariables enviromentVariables);

    EnvironmentVariables getEnvironmentVariables();

    /**
     * the name of project test automation framework
     */
    String getName();
}
