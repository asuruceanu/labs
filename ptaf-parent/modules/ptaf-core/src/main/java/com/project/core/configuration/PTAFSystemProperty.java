package com.project.core.configuration;

import com.project.core.util.EnvironmentVariables;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public enum PTAFSystemProperty {

    /**
     * Allow you to override the default properties location for tproperties file
     */
    PROPERTYES,

    /**
     * The name if project test automation framework
     */
    PTAF_NAME;

    private String propertyName;

    private PTAFSystemProperty(final String propertyName) {
        this.propertyName = propertyName;
    }

    private PTAFSystemProperty() {
        this.propertyName = name().replace("_", ".").toLowerCase();
    }

    public String getPropertyName() {
        return propertyName;
    }

    @Override
    public String toString() {
        return propertyName;
    }

    public String from(EnvironmentVariables environmentVariables) {
        return from(environmentVariables, null);
    }

    public String from(EnvironmentVariables environmentVariables, String defaultValue) {
        Optional<String> newPropertyValue = Optional.ofNullable(environmentVariables.getProperty(getPropertyName()));

        if (isDefined(newPropertyValue)) {
            return newPropertyValue.get();
        } else {
            return defaultValue;
        }
    }

    public boolean isDefined(Optional<String> newPropertyValue) {
        return newPropertyValue.isPresent() && StringUtils.isNoneEmpty(newPropertyValue.get());
    }

    public int integerFrom(EnvironmentVariables environmentVariables) {
        return integerFrom(environmentVariables, 0);
    }

    public int integerFrom(EnvironmentVariables environmentVariables, int defaultValue) {
        Optional<String> newPropertyValue = Optional.ofNullable(environmentVariables.getProperty(getPropertyName()));

        if (isDefined(newPropertyValue)) {
            return Integer.valueOf(newPropertyValue.get());
        } else {
            return defaultValue;
        }
    }

    public boolean booleanFrom(EnvironmentVariables environmentVariables) {
        return booleanFrom(environmentVariables, false);
    }

    public Boolean booleanFrom(EnvironmentVariables environmentVariables, Boolean defaultValue) {
        Optional<String> newPropertyValue = Optional.ofNullable(environmentVariables.getProperty(getPropertyName()));

        if (isDefined(newPropertyValue)) {
            return Boolean.valueOf(newPropertyValue.get());
        } else {
            return defaultValue;
        }
    }

    public boolean isDefinedIn(EnvironmentVariables environmentVariables) {
        return StringUtils.isNoneEmpty(from(environmentVariables));
    }
}
