package com.project.core.configuration;

import com.google.common.base.CharMatcher;
import com.google.common.collect.Maps;
import com.project.core.util.EnvironmentVariables;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import java.util.HashMap;

/**
 * Centralized configuration of the test runner.
 * Most configuratuin elements can be set using system properties.
 */
public class SystemPropertiesPTAFConfiguration implements PTAFConfiguration {

    private static final CharMatcher SEPARATOR = CharMatcher.anyOf(";");
    private final EnvironmentVariables evironmetnVariables;

    /**
     * {@inheritDoc}
     */
    @Inject
    public SystemPropertiesPTAFConfiguration(EnvironmentVariables evironmetnVariables) {
        this.evironmetnVariables = evironmetnVariables;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setIfUdefined(String property, String value) {
        if (getEnvironmentVariables().getProperty(property) == null) {
            getEnvironmentVariables().setProperty(property, value);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PTAFConfiguration copy() {
        return withEnviromentVariables(evironmetnVariables);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PTAFConfiguration withEnviromentVariables(EnvironmentVariables enviromentVariables) {
        return new SystemPropertiesPTAFConfiguration(enviromentVariables.copy());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnvironmentVariables getEnvironmentVariables() {
        return evironmetnVariables;
    }

    @Override
    public String getName() {
        return getEnvironmentVariables().getProperty(PTAFSystemProperty.PTAF_NAME.getPropertyName(), "ptaf");
    }

    private HashMap<String, Object> addConfigOptionsMapValues(Iterable<String> configOptionsValues) {
        HashMap<String, Object> configOptionsMap = Maps.newHashMap();

        for (String configOption : configOptionsValues) {

            Token token = new Token(configOption);
            if (token.isDefined()) {
                configOptionsMap.put(token.getName(), asObject(token.getValue()));
            }
        }

        return configOptionsMap;
    }

    private Object asObject(String value) {
        if (StringUtils.isNumeric(value)) {
            return Integer.parseInt(value);
        } else if (value.toLowerCase().equals("true") || value.toLowerCase().equals("false")) {
            return Boolean.parseBoolean(value);
        }

        return value;
    }

    private static class Token {
        private final String name;
        private final String value;

        private Token(String option) {
            int colonIndex = option.indexOf(":");

            if (colonIndex >= 0) {
                name = option.substring(0, colonIndex + 1);
                value = option.substring(colonIndex + 1);
            } else {
                name = option;
                value = null;
            }
        }

        public boolean isDefined() {
            return (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(value));
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }
}
