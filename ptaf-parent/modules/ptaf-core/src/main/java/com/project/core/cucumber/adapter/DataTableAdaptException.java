package com.project.core.cucumber.adapter;

public class DataTableAdaptException extends RuntimeException {

    private static final long serialVersionUID = 102768520587282095L;

    public DataTableAdaptException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    public DataTableAdaptException(final String msg) {
        super(msg);
    }
}
