package com.project.core.cucumber.adapter;

import cucumber.api.DataTable;

import java.util.List;

public interface DataTableAdapter<T> {
    List<T> adapt(DataTable dt);
}
