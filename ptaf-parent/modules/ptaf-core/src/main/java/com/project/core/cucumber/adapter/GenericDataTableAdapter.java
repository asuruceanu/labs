package com.project.core.cucumber.adapter;

import cucumber.api.DataTable;

import java.util.List;

public class GenericDataTableAdapter extends ReflectiveDataTableAdapter {

    private final Class<? extends TableInfo> infoClass;

    public GenericDataTableAdapter(final Class<? extends TableInfo> infoClass) {
        this.infoClass = infoClass;
    }


    @SuppressWarnings("unchecked")
    @Override
    public List<? extends TableInfo> adapt(final DataTable dt) {
        return doAdapt(infoClass, dt);
    }

}

