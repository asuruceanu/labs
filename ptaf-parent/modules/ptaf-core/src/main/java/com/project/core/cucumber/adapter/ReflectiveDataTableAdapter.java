package com.project.core.cucumber.adapter;

import com.google.common.collect.Maps;
import cucumber.api.DataTable;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

abstract class ReflectiveDataTableAdapter<T extends TableInfo> implements DataTableAdapter<T> {

    private final Map<String, String> aliases = Maps.newHashMap();


    protected List<T> adaptReflectively(final DataTable dt) {
        Class<T> infoClass;

        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
            try {
                infoClass = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
            } catch (final Exception e) {
                return null;
            }
        } else {
            return null;
        }
        return doAdapt(infoClass, dt);
    }

    protected List<T> doAdapt(final Class<T> infoClass, final DataTable dt) {
        final List<Map<String, String>> rows = dt.asMaps(String.class, String.class);

        final List<T> resoults = new ArrayList<>();
        for (final Map<String, String> row : rows) {
            T info;
            try {
                info = infoClass.newInstance();
            } catch (final Exception e1) {
                continue;
            }
            for (final Map.Entry<String, String> cell : row.entrySet()) {
                final String colHeader = cell.getKey();
                final String value = cell.getValue();
                final Field field = determineField(info, colHeader);
                if (field != null && StringUtils.isNotBlank(value)) {
                    try {
                        field.setAccessible(true);
                        if (Integer.class.equals(field.getType()) || Integer.TYPE.equals(field.getType())) {
                            field.set(info, Integer.valueOf(value));
                        } else if (Long.class.equals(field.getType()) || Long.TYPE.equals(field.getType())) {
                            field.set(info, Long.valueOf(value));
                        } else if (Boolean.class.equals(field.getType()) || Boolean.TYPE.equals(field.getType())) {
                            field.set(info, "Y".equalsIgnoreCase(value) || "1".equals(value) || "true".equalsIgnoreCase(value));
                        } else if (BigDecimal.class.equals(field.getType())) {
                            field.set(info, new BigDecimal(value));
                        } else {
                            field.set(info, value);
                        }
                    } catch (final NumberFormatException nfe) {
                        throw new DataTableAdaptException(String.format("Value %s is not correctly formatted for $s field %s", value, field.getType()
                                .getSimpleName(), field.getName(), nfe));
                    } catch (final Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            resoults.add(info);
        }
        return resoults;
    }

    private Field determineField(final T info, final String fieldToBeFound) {
        final String actualField = getAlias(fieldToBeFound);

        final List<Field> declaredFields = getDeclaredFields(info.getClass());
        for (final Field declaredField : declaredFields) {
            if (StringUtils.endsWithIgnoreCase(declaredField.getName(), actualField)) {
                return declaredField;
            }
        }
        throw new DataTableAdaptException(String.format("Field %s not found in %s ", fieldToBeFound, info.getClass().getCanonicalName()));
    }

    private List<Field> getDeclaredFields(final Class<?> clazz) {
        final List<Field> result = new ArrayList<>();
        result.addAll(Arrays.asList(clazz.getDeclaredFields()));
        if (clazz.getSuperclass() != null) {
            result.addAll(getDeclaredFields(clazz.getSuperclass()));
        }

        return result;
    }

    protected void addAlias(final String alias, final String actualField) {
        aliases.put(alias, actualField);
    }

    private String getAlias(final String columnName) {
        if (aliases.containsKey(columnName)) {
            return aliases.get(columnName);
        }
        return columnName;
    }

}
