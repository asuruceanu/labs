package com.project.core.cucumber.adapter.factory;

import com.project.core.cucumber.adapter.DataTableAdapter;
import com.project.core.cucumber.adapter.GenericDataTableAdapter;
import com.project.core.cucumber.adapter.TableInfo;

import java.util.HashMap;
import java.util.Map;

public class DataTableAdapterFactory {
    private static Map<Class<? extends TableInfo>, DataTableAdapter<? extends TableInfo>> adapters = new HashMap<>();

    @SuppressWarnings("unchecked")
    public static <T extends TableInfo> DataTableAdapter<T> getAdapter(final Class<T> type) {
        if (adapters.containsKey(type)) {
            return (DataTableAdapter<T>) adapters.get(type);
        } else {
            return new GenericDataTableAdapter(type);
        }
    }
}
