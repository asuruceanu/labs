package com.project.core.endpoint;

import org.springframework.beans.factory.BeanNameAware;

public abstract class AbstractEndpoint implements Endpoint, BeanNameAware {

    /**
     * Endpoint configuration
     */
    private final EndpointConfiguration endpointConfiguration;

    /**
     * Endpoint name usually the Spring bean id
     */
    private String name = getClass().getSimpleName();

    /**
     * Default constructor using endpoint configuration.
     *
     * @param endpointConfiguration
     */
    public AbstractEndpoint(EndpointConfiguration endpointConfiguration) {
        this.endpointConfiguration = endpointConfiguration;
    }

    @Override
    public EndpointConfiguration getEndpointConfiguration() {
        return this.endpointConfiguration;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }
}
