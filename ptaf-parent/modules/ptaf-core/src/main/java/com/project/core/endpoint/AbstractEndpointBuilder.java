package com.project.core.endpoint;

import com.project.core.exceptions.PTAFRuntimeException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author Dumitru.Alexei
 */
public abstract class AbstractEndpointBuilder<T extends Endpoint> implements EndpointBuilder<T> {

    /**
     * Sets the Endpoint name.
     *
     * @param endpointName
     * @return
     */
    public AbstractEndpointBuilder<T> name(String endpointName) {
        getEndpoint().setName(endpointName);
        return this;
    }


    /**
     * Initialize the endpoint.
     *
     * @return
     */
    public AbstractEndpointBuilder<T> initialize() {
        if (getEndpoint() instanceof InitializingBean) {
            try {
                ((InitializingBean) ((InitializingBean) getEndpoint())).afterPropertiesSet();
            } catch (Exception e) {
                throw new PTAFRuntimeException("Failed to initialize Endpoint", e);
            }
        }
        return this;
    }

    /**
     * Sets the Spring application context
     *
     * @param applicationContext
     * @return
     */
    public AbstractEndpointBuilder<T> applicationContext(ApplicationContext applicationContext) {
        if (getEndpoint() instanceof ApplicationContextAware) {
            ((ApplicationContextAware) getEndpoint()).setApplicationContext(applicationContext);
        }

        return this;
    }


    @Override
    public T build() {
        return getEndpoint();
    }

    /**
     * Gets the target endpoint instance.
     *
     * @return
     */
    protected abstract T getEndpoint();
}
