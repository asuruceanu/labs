package com.project.core.endpoint;

public abstract class AbstractEndpointConfiguration implements EndpointConfiguration {

    /**
     * Send/receive timeout setting
     */
    private long timeout = 5000L;

    /**
     * Gets the timeout for sending and receiving messages.
     *
     * @return
     */
    public Long getTimeout() {
        return timeout;
    }

    /**
     * Sets the timeout for sending and receiving messages..
     *
     * @param timeout
     */
    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

}
