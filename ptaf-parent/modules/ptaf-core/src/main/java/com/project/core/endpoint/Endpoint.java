package com.project.core.endpoint;

/**
 * Endpoint interface defines basic send and receive operations on a message endpoint.
 *
 * @author Dumitru.Alexei
 */
public interface Endpoint {
    /**
     * Gets the endpoint configuration holding all endpoint specific propeties such as
     * endpoint uri, connection timaout, ports, etc.
     *
     * @return
     */
    EndpointConfiguration getEndpointConfiguration();

    /**
     * Gets the endpoint name usually the Spring bean name.
     *
     * @return
     */
    String getName();

    /**
     * Sets the endpoint name.
     *
     * @param name
     */
    void setName(String name);
}
