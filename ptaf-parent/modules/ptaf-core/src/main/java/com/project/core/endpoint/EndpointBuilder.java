package com.project.core.endpoint;

public interface EndpointBuilder<T extends Endpoint> {
    /**
     * Builds the endpoint.
     *
     * @return the T
     */
    T build();
}
