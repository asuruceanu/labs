package com.project.core.endpoint;

public interface EndpointConfiguration {

    /**
     * Gets the timeout either for sending or receiving messages
     */
    Long getTimeout();

    /**
     * Sets the timeout setting for this endpoint.
     *
     * @param timeout
     */
    void setTimeout(long timeout);

}
