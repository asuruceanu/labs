package com.project.core.exceptions;

/**
 * Basic custom runtime exception for all errors in PTAF
 *
 * @author Dimotru.Alexei
 */
public class PTAFRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1l;

    public PTAFRuntimeException() {
    }

    public PTAFRuntimeException(String message) { super(message); }

    public PTAFRuntimeException(Throwable cause) { super(cause); }

    public PTAFRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getMessage() {return super.getMessage(); }
}
