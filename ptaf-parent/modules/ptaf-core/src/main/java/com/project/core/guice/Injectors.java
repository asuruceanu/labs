package com.project.core.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public class Injectors {

    private static Injector injector;

    public static synchronized Injector getInjector() {
        if (injector == null) {
            injector = Guice.createInjector(new PTAFModule());
        }
        return injector;
    }

    public static synchronized Injector getInjector(Module module) {
        if (injector == null) {
            injector = Guice.createInjector(module);
        }
        return injector;
    }
}
