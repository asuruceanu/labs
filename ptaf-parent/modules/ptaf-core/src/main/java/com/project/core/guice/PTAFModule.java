package com.project.core.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.project.core.configuration.SystemPropertiesPTAFConfiguration;
import com.project.core.configuration.PTAFConfiguration;
import com.project.core.util.EnvironmentVariables;
import com.project.core.util.SystemEnvironmentVariables;

public class PTAFModule extends AbstractModule {

    @Override
    protected void configure() {
        this.bind(PTAFConfiguration.class).to(SystemPropertiesPTAFConfiguration.class).in(Singleton.class);
    }

    @Provides
    @Singleton
    public EnvironmentVariables provideEnvironmentVariables() {
        return SystemEnvironmentVariables.createEnviromentVariables();
    }
}
