package com.project.core.message;

import com.project.core.exceptions.PTAFRuntimeException;
import com.project.core.util.TypeConversionUtils;
import org.springframework.util.CollectionUtils;

import java.util.*;

public class DefaultMessage implements Message {

    /**
     * Serial
     */
    private static final long serialVersionUID = 1904510540660648952L;

    /**
     * Message headers
     */
    private final Map<String, Object> headers;

    /**
     * message payload object
     */
    private Object payload;

    /**
     * Optional list of headers data
     */
    private List<String> headerData = new ArrayList<String>();

    /**
     * The message name for internal use
     */
    private String name;

    /**
     * Empty constructor initializing with empty message payload.
     */
    public DefaultMessage() {
        this("");
    }

    /**
     * construct copy of given message.
     *
     * @param message
     */
    public DefaultMessage(Message message) {
        this(message.getPayload());
        this.setName((message.getName()));
        this.headers.putAll(message.getHeaders());

        for (String data : message.getHeaderData()) {
            addHeaderData(data);
        }
    }

    /**
     * Default constructor using just message payload.
     *
     * @param payload
     */
    public DefaultMessage(Object payload) {
        this(payload, new LinkedHashMap<>());
    }

    /**
     * Default constructor using payload and headers
     *
     * @param payload
     * @param headers
     */
    public DefaultMessage(Object payload, Map<String, Object> headers) {
        this.payload = payload;
        this.headers = headers;

        this.headers.put(MessageHeaders.ID, UUID.randomUUID().toString());
        this.headers.put(MessageHeaders.TIMESTAMP, System.currentTimeMillis());
    }

    @Override
    public String toString() {
        if (CollectionUtils.isEmpty(headerData)) {
            return String.format("%s [payload: %s]", getClass().getSimpleName().toUpperCase(), getPayload(String.class).trim(), headers);
        } else {
            return String.format("%s [payload: %s][headers: %s][header-data: %s]", getClass().getSimpleName().toUpperCase(), getPayload(String.class).trim(), headers, headerData);
        }
    }

    @Override
    public DefaultMessage setHeader(String headerName, Object headerValue) {
        if (headerName.equals(MessageHeaders.ID)) {
            throw new PTAFRuntimeException(("Not allowed to set reserved message header: " + MessageHeaders.ID));
        }

        headers.put(headerName, headerValue);
        return this;
    }

    public Long getTimestamp() {
        return (Long) headers.get(MessageHeaders.TIMESTAMP);
    }

    @Override
    public Object getHeader(String headerName) {
        return headers.get(headerName);
    }

    @Override
    public void removeHeader(String headerName) {
        if (headerName.equals(MessageHeaders.ID)) {
            throw new PTAFRuntimeException(("Not allowed to set reserved message header: " + MessageHeaders.ID));
        }

        headers.remove(headerName);
    }

    @Override
    public DefaultMessage addHeaderData(String headerData) {
        this.headerData.add(headerData);
        return this;
    }

    @Override
    public List<String> getHeaderData() {
        return headerData;
    }

    @Override
    public <T> T getPayload(Class<T> type) {
        return TypeConversionUtils.convertIfNecessary(getPayload(), type);
    }

    @Override
    public Object getPayload() {
        return payload;
    }

    @Override
    public void setPayload(Object payload) {
        this.payload = payload;
    }

    @Override
    public Map<String, Object> getHeaders() {
        return headers;
    }

    @Override
    public String getId() {
        return headers.get(MessageHeaders.ID).toString();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }


}
