package com.project.core.message;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface Message extends Serializable {

    /**
     * Gets the unique message id.
     *
     * @return
     */
    String getId();

    /**
     * Gets the message name for internal use.
     *
     * @return
     */
    String getName();

    /**
     * Sets the message name for internal use.
     *
     * @param name
     */
    void setName(String name);

    /**
     * Gets the message header valuee by its header name.
     *
     * @param headerName
     * @return
     */
    Object getHeader(String headerName);

    /**
     * Set new header entry in message header list.
     *
     * @param headerName
     * @param headerValue
     * @return
     */
    Message setHeader(String headerName, Object headerValue);

    /**
     * Removes the message header if it not a reserved message header such as unique message id.
     *
     * @param headerName
     */
    void removeHeader(String headerName);

    /**
     * Adds new header data.
     *
     * @param headerData
     * @return
     */
    Message addHeaderData(String headerData);

    /**
     * gets the list of header data in this message.
     *
     * @return
     */
    List<String> getHeaderData();

    /**
     * Gets message headers.
     *
     * @return
     */
    Map<String, Object> getHeaders();

    /**
     * Gets message payload with required type conversion.
     *
     * @param type
     * @param <T>
     * @return
     */
    <T> T getPayload(Class<T> type);

    /**
     * Gets the message payload
     *
     * @return
     */
    Object getPayload();

    /**
     * Sets the message payload.
     *
     * @param payload
     */
    void setPayload(Object payload);
}
