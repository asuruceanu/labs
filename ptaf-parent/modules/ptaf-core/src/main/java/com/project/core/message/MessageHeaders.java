package com.project.core.message;

public final class MessageHeaders {

    /**
     * Common header name prefix.
     */
    public static final String PREFIX = "ptaf_";

    /**
     * Message related header prefix.
     */
    public static final String MESSAGE_PREFIX = PREFIX + "message_";

    /**
     * Unique message id.
     */
    public static final String ID = MESSAGE_PREFIX + "id";

    /**
     * Time message was created.
     */
    public static final String TIMESTAMP = MESSAGE_PREFIX + "timestamp";

    /**
     * Header indicating the message type (e.g. xml, json, csv, plaintext, etc)
     */
    public static final String MESSAGE_TYPE = MESSAGE_PREFIX + "type";

    /**
     * Synchronous message correlation
     */
    public static final String MESSAGE_CORELATION_KEY = PREFIX + "message_correlation";

    /**
     * prevent instantiation.
     */
    private MessageHeaders() {
    }
}
