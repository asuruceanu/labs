package com.project.core.messaging;

import com.project.core.message.Message;

public interface JmxProducer {

    /**
     * sends the message.
     *
     * @param message
     * @return
     */
    Message send(Message message);

    /**
     * Get the producer name.
     *
     * @return
     */
    String getName();
}
