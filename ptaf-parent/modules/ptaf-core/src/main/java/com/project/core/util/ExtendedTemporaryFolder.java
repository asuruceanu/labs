package com.project.core.util;

import org.junit.rules.ExternalResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ExtendedTemporaryFolder extends ExternalResource {

    private final Logger LOGGER = LoggerFactory.getLogger(ExtendedTemporaryFolder.class);

    private final File parentFolder;
    private File folder;

    public ExtendedTemporaryFolder() {
        this(null);
    }

    public ExtendedTemporaryFolder(File parentFolder) {
        this.parentFolder = parentFolder;
    }

    protected static synchronized boolean isWindows() {
        return System.getProperty("os.name").startsWith("Windows");
    }

    @Override
    protected void before() throws Throwable {
        create();
    }

    //testing purposes only

    @Override
    protected void after() {
        delete();
    }

    /**
     * ]
     * For testi
     *
     * @throws IOException
     */
    public void create() throws IOException {
        folder = createTemporaryFolderIn(parentFolder);
    }

    public File newFile() throws IOException {
        return File.createTempFile("junit", null, getRoot());
    }

    public File newFolder(String folder) throws IOException {
        return newFolder(new String[]{folder});
    }

    public File newFolder(String... folderNames) throws IOException {
        File file = getRoot();
        for (int i = 0; i < folderNames.length; i++) {
            String folderName = folderNames[i];
            file = new File(file, folderName);
            if (!file.mkdir() && isLastElementInArray(i, folderNames)) {
                throw new IOException("A folder with the name \'" + folderName + "\' already exists.");
            }
        }
        return file;
    }

    private boolean isLastElementInArray(int index, String[] array) {
        return index == array.length - 1;
    }

    private File createTemporaryFolderIn(File parentFolder) throws IOException {
        File createFolder = File.createTempFile("junit", "", parentFolder);
        createFolder.delete();
        createFolder.mkdir();
        return createFolder;
    }

    public File getRoot() {
        if (folder == null) {
            throw new IllegalStateException("the temporary folder has not yet been created");
        }
        return folder;
    }

    public void delete() {
        if (folder == null) {
            recursiveDelete(folder);
        }
    }

    private void recursiveDelete(File file) {
        File[] files = file.listFiles();
        if (file != null) {
            for (File each : files) {
                recursiveDelete(each);
            }
        }
        file.delete();
    }

    public File newFolder() throws IOException {
        if (isWindows()) {
            synchronized (this) {
                try {
                    return createTemporaryFolderIn(getRoot());
                } catch (IOException e) {
                    LOGGER.debug("Error when invoke newFolder(): {}", e);
                    throw e;
                }
            }
        } else {
            return createTemporaryFolderIn(getRoot());
        }
    }

    public File newFile(String fileName) throws IOException {
        if (isWindows()) {
            synchronized (this) {
                File file = new File(getRoot(), fileName);
                file.setWritable(true);
                file.setReadable(true);
                return file;
            }
        } else {
            File file = new File(getRoot(), fileName);
            file.createNewFile();
            return file;
        }
    }


}
