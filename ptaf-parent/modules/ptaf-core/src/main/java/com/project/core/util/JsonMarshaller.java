package com.project.core.util;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.io.InputStream;

public class JsonMarshaller {

    public static String marshallFlat(Object object) throws Exception {
        ObjectMapper mapper = getObjectMapper(false);
        return mapper.writeValueAsString(object);
    }

    public static String marshallPrettyPrint(Object object) throws IOException {
        ObjectMapper mapper = getObjectMapper(true);
        return mapper.writeValueAsString(object);
    }

    private static ObjectMapper getObjectMapper(boolean prettyPrint) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        if(prettyPrint) {
            mapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);
        }
        return mapper;
    }

    public static <T> T unmarshall(InputStream stream, Class<?> target) throws IOException{
        ObjectMapper objectMapper = getObjectMapper(true);
        return objectMapper.readValue(stream, objectMapper.getTypeFactory().constructType(target));
    }
}
