package com.project.core.util;

import java.io.IOException;

/**
 * Loads configuration values from local files into the enviroment variables.
 */
public interface LocalPreferences {
    void loadPreferences() throws IOException;
}
