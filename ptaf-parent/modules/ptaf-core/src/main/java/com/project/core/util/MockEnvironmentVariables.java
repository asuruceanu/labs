package com.project.core.util;


import ch.lambdaj.Lambda;
import ch.lambdaj.function.convert.DefaultStringConverter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Properties;

public class MockEnvironmentVariables implements EnvironmentVariables {

    private Properties properties = new Properties();
    private Properties values = new Properties();

    public MockEnvironmentVariables() {
        this.properties.setProperty("user.home", System.getProperty("user.home"));
    }

    protected MockEnvironmentVariables(Properties properties) {
        this.properties = PropertiesUtil.copyOf(properties);
    }

    protected MockEnvironmentVariables(Properties properties, Properties value) {
        this.properties = PropertiesUtil.copyOf(properties);
        this.values = PropertiesUtil.copyOf(System.getProperties());
    }

    public static EnvironmentVariables fromSystemEnvironment() {
        return new MockEnvironmentVariables(System.getProperties());
    }

    public boolean propertySetIsEmpty() {
        return properties.isEmpty();
    }

    @Override
    public String getValue(String name) {
        return values.getProperty(name);
    }

    @Override
    public String getValue(Enum<?> property) {
        return getValue(property.toString());
    }

    @Override
    public String getValue(String name, String defaultValue) {
        return values.getProperty(name, defaultValue);
    }

    @Override
    public String getValue(Enum<?> property, String defaultValue) {
        return getValue(property.toString(), defaultValue);
    }

    @Override
    public Integer getPropertyAsInteger(String name, Integer defaultValue) {
        String value = (String) properties.get(name);
        if (StringUtils.isNumeric(value)) {
            return Integer.parseInt(properties.getProperty(name));
        } else {
            return defaultValue;
        }
    }

    @Override
    public Integer getPropertyAsInteger(Enum<?> property, Integer defaultValue) {
        return getPropertyAsInteger(property.toString(), defaultValue);
    }

    @Override
    public Long getPropertyAsLong(String name, Long defaultValue) {
        String value = (String) properties.get(name);
        if (StringUtils.isNumeric(value)) {
            return Long.parseLong(properties.getProperty(name));
        } else {
            return defaultValue;
        }
    }

    @Override
    public Boolean getPropertyAsBoolean(String name, boolean defaultValue) {
        if (properties.getProperty(name) == null) {
            return defaultValue;
        } else {
            return Boolean.parseBoolean(properties.getProperty(name, "false"));
        }
    }

    @Override
    public Boolean getPropertyAsBoolean(Enum<?> property, boolean defaultValue) {
        return getPropertyAsBoolean(property.toString(), defaultValue);
    }

    @Override
    public String getProperty(String name) {
        return properties.getProperty(name);
    }

    @Override
    public String getProperty(Enum<?> property) {
        return getProperty(property.toString());
    }

    @Override
    public String getProperty(String name, String defaultValue) {
        return properties.getProperty(name, defaultValue);
    }

    @Override
    public String getProperty(Enum<?> property, String value) {
        return getProperty(property.toString(), value);
    }

    @Override
    public void setProperty(String name, String value) {
        properties.setProperty(name, value);
    }

    @Override
    public void clearProperty(String name) {
        properties.remove(name);
    }

    @Override
    public EnvironmentVariables copy() {
        return new MockEnvironmentVariables(properties, values);
    }

    @Override
    public List<String> getKeys() {
        return Lambda.convert(properties.keySet(), new DefaultStringConverter());
    }

    public void setValues(String name, String value) {
        values.setProperty(name, value);
    }
}
