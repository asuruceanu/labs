package com.project.core.util;

import com.google.inject.Inject;
import com.project.core.configuration.PTAFSystemProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;


/**
 * Loads PTAF preferences from a local file called ptaf.properties
 */
public class PropertiesFileLocalPreferences implements LocalPreferences {
    private final EnvironmentVariables environmentVariables;
    private final Logger LOGGER = LoggerFactory.getLogger((PropertiesFileLocalPreferences.class));
    private File workingDirectory;
    private File homeDirectory;

    @Inject
    public PropertiesFileLocalPreferences(EnvironmentVariables environmentVariables) {
        this.environmentVariables = environmentVariables;
        this.homeDirectory = new File(System.getProperty("user.home"));
        this.workingDirectory = new File(System.getProperty("user.dir"));
    }

    public File getHomeDirectory() {
        return homeDirectory;
    }

    public void setHomeDirectory(File homeDirectory) {
        this.homeDirectory = homeDirectory;
    }

    public void loadPreferences() throws IOException {
        updatePreferencesFrom(preferencesFileInHomeDirectory());
        updatePreferencesFrom(preferencesFileInWorkingDirectory());
        updatePreferencesFrom(preferencesFileWithAbsolutePath());
        updatePreferencesFromClasspath();
    }

    private void updatePreferencesFromClasspath() throws IOException {
        InputStream propertiesOnClasspath = null;
        try {
            propertiesOnClasspath = Thread.currentThread().getContextClassLoader().getResourceAsStream(defaultPropertiesFileName());

            if (propertiesOnClasspath != null) {
                Properties localPreferences = new Properties();
                localPreferences.load(propertiesOnClasspath);
                setUndefinedSystemPropertiesFrom(localPreferences);
            }
        } finally {
            if (propertiesOnClasspath != null) {
                propertiesOnClasspath.close();
            }
        }
    }

    private void updatePreferencesFrom(File preferencesFile) throws IOException {
        if (preferencesFile.exists()) {
            Properties localPreferences = new Properties();
            LOGGER.debug("LOADING LOCAL PROPERTIES FROM {}", preferencesFile.getAbsoluteFile());
            localPreferences.load(new FileInputStream(preferencesFile));
            setUndefinedSystemPropertiesFrom(localPreferences);
        }
    }

    private void setUndefinedSystemPropertiesFrom(Properties localPreferences) {
        Enumeration propertyNames = localPreferences.propertyNames();
        while (propertyNames.hasMoreElements()) {
            String propertyName = (String) propertyNames.nextElement();
            String localPropertyValue = localPreferences.getProperty(propertyName);
            String currentPropertyValue = environmentVariables.getProperty(propertyName);

            if ((currentPropertyValue == null) && (localPropertyValue != null)) {
                LOGGER.debug(propertyName + " = " + localPropertyValue);
                environmentVariables.setProperty(propertyName, localPropertyValue);
            }
        }
    }

    private File preferencesFileInHomeDirectory() {
        return new File(homeDirectory, defaultPropertiesFileName());
    }

    private File preferencesFileInWorkingDirectory() {
        return new File(workingDirectory, defaultPropertiesFileName());
    }

    private File preferencesFileWithAbsolutePath() {
        return new File(defaultPropertiesFileName());
    }

    private String defaultPropertiesFileName() {
        return PTAFSystemProperty.PROPERTYES.from(environmentVariables, "ptaf.properties");
    }


}
