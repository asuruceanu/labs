package com.project.core.util;

import com.project.core.PTAF;
import com.project.core.exceptions.PTAFRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.SimpleTypeConverter;
import org.springframework.core.io.InputStreamResource;
import org.springframework.util.StringUtils;
import org.springframework.xml.transform.StringSource;
import org.w3c.dom.Node;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.*;

public abstract class TypeConversionUtils {

    private static Logger log = LoggerFactory.getLogger(TypeConversionUtils.class);

    /**
     * prevent instantiation.
     */
    private TypeConversionUtils() { super(); }

    public static <T> T convertIfNecessary(Object target, Class<T> type) {
        if(type.isInstance(target)) {
            return type.cast(target);
        }

        if(Source.class.isAssignableFrom(type)) {
            if (target.getClass().isAssignableFrom(String.class)) {
                return (T) new StringSource(String.valueOf(target));
            }else if(target.getClass().isAssignableFrom(Node.class)) {
                return (T) new DOMSource((Node) target);
            } else if (target.getClass().isAssignableFrom(InputStreamResource.class)) {
                try {
                    return (T) new StreamSource(((InputStreamResource) target).getInputStream());
                } catch (IOException e) {
                    log.warn("Failed to create stream source from object ", e);
                }
            }
        }

        if( Map.class.isAssignableFrom(type)) {
            String mapString = String.valueOf(target);

            Properties props = new Properties();
            try {
                props.load(new StringReader(mapString.substring(1, mapString.length() - 1 ).replace(",", "\n")));
            } catch (IOException e) {
                throw new PTAFRuntimeException("Failed to reconstruct object of type map,", e);
            }
            Map<String, Object> map = new LinkedHashMap<>();
            for (Map.Entry<Object, Object> entry : props.entrySet()) {
                map.put(entry.getKey().toString(), entry.getValue());
            }

            return (T) map;
        }

        if(String[].class.isAssignableFrom(type)) {
            String arrayString = String.valueOf(target).replaceAll("^\\[", "").replaceAll("\\]$", "").replaceAll(",\\s", ",");
            return (T) StringUtils.commaDelimitedListToStringArray(String.valueOf(arrayString));
        }

        if (List.class.isAssignableFrom(type)) {
            String listString = String.valueOf(target).replaceAll("^\\[", "").replaceAll("\\]$", "").replaceAll(",\\s", ",");
            return (T) Arrays.asList(StringUtils.commaDelimitedListToStringArray(String.valueOf(listString)));
        }

        if (InputStream.class.isAssignableFrom(type)) {
            if (target instanceof byte[]) {
                return (T) new ByteArrayInputStream((byte[]) target);
            } else if(target instanceof String) {
                try {
                    return (T) new ByteArrayInputStream(String.valueOf(target).getBytes(PTAF.PTAF_FILE_ENCODING));
                } catch (UnsupportedEncodingException e) {
                    return (T) new ByteArrayInputStream(String.valueOf(target).getBytes());
                }
            } else {
                try {
                    return (T) new ByteArrayInputStream(target.toString().getBytes(PTAF.PTAF_FILE_ENCODING));
                } catch (UnsupportedEncodingException e) {
                    return (T) new ByteArrayInputStream(target.toString().getBytes());
                }
            }
        }
        try {
            return new SimpleTypeConverter().convertIfNecessary(target, type);
        } catch (ConversionNotSupportedException e) {
            if(String.class.equals(type)) {
                log.warn(String.format("Using object toStrong representation: %s", e.getMessage()));
                return (T) target.toString();
            }
            throw e;
        }
    }


    public static <T> T convertStringToType(String value, Class<T> type) {
        if(type.isAssignableFrom(String.class)) {
            return (T) value;
        } else if (type.isAssignableFrom(int.class) || type.isAssignableFrom(Integer.class)) {
            return (T) Integer.valueOf(value);
        } else if (type.isAssignableFrom(short.class) || type.isAssignableFrom(Short.class)) {
            return (T) Short.valueOf(value);
        } else if (type.isAssignableFrom(byte.class) || type.isAssignableFrom(Byte.class)) {
            return (T) Byte.valueOf(value);
        } else if (type.isAssignableFrom(long.class) || type.isAssignableFrom(Long.class)) {
            return (T) Long.valueOf(value);
        } else if (type.isAssignableFrom(boolean.class) || type.isAssignableFrom(Boolean.class)) {
            return (T) Boolean.valueOf(value);
        } else if (type.isAssignableFrom(float.class) || type.isAssignableFrom(Float.class)) {
            return (T) Float.valueOf(value);
        } else if (type.isAssignableFrom(double.class) || type.isAssignableFrom(Double.class)) {
            return (T) Double.valueOf(value);
        }

        throw new PTAFRuntimeException(String.format("Unable to convert '%s' to required type '%s'", value,type.getName() ));
    }
}
