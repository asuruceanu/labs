package com.project.core.configuration;

import com.project.core.format.BaseTestSetUp;
import com.project.core.util.EnvironmentVariables;
import com.project.core.util.ExtendedTemporaryFolder;
import com.project.core.util.MockEnvironmentVariables;
import com.project.core.util.PropertiesFileLocalPreferences;
import com.project.core.guice.Injectors;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class LoadingPreferencesFromALocalIPropertiesFileTest extends BaseTestSetUp{
    @Rule
    public ExtendedTemporaryFolder temporaryFolder = new ExtendedTemporaryFolder();

    File homeDirectory;
    File ptafPropertiesFile;
    EnvironmentVariables environmentVariables;
    PropertiesFileLocalPreferences localPreferences;

    @Before
    public void setupDirectories() throws IOException {
        environmentVariables = new MockEnvironmentVariables();
        localPreferences = new PropertiesFileLocalPreferences(environmentVariables);

        homeDirectory = temporaryFolder.newFolder();
        localPreferences.setHomeDirectory(homeDirectory);
    }

    @Test
    public void the_default_preferences_directory_is_the_users_home_directory() throws Exception {
        PropertiesFileLocalPreferences localPreferences = new PropertiesFileLocalPreferences(environmentVariables);

        String homeDirectory = System.getProperty("user.home");

        assertThat(localPreferences.getHomeDirectory().getParent(), is(homeDirectory));
    }

    @Test
    public void should_load_property_values_from_local_preferences() throws Exception{
        writeToPropertiesFile("ptaf.name = PTAF awesome!");

        localPreferences.setHomeDirectory(homeDirectory);

        localPreferences.loadPreferences();

        assertThat(environmentVariables.getProperty("ptaf.name"), is("PTAF awesome!"));
    }

    @Test
    public void local_preferences_should_be_loaded_with_the_enviroment_variables() {
        EnvironmentVariables loadedEnviromentVariables = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
        assertThat(loadedEnviromentVariables.getProperty("ptaf.name"), is("Vocalink Test Automation Framework"));
    }

    private String writeToPropertiesFile(String... lines) throws IOException, InterruptedException {
        return writeToPropertiesFileCalled("ptaf.properties", lines);
    }

    @SuppressWarnings("static-access")
    private String writeToPropertiesFileCalled(String fileName, String... lines) throws IOException, InterruptedException {
        ptafPropertiesFile = new File(homeDirectory, fileName);
        ptafPropertiesFile.setReadable(true);
        ptafPropertiesFile.setWritable(true);
        ptafPropertiesFile.setExecutable(true);

        try {
            ptafPropertiesFile.createNewFile();
        } catch (IOException e) {
            System.err.println(e);
        }
        Thread.currentThread().sleep(100);
        FileWriter outFile = new FileWriter(ptafPropertiesFile);
        PrintWriter out = new PrintWriter(outFile);
        for (String line : lines) {
            out.println(line);
        }
        out.close();
        return ptafPropertiesFile.getAbsolutePath();
    }
}
