package com.project.core.configuration;

import com.project.core.util.MockEnvironmentVariables;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ManagingGlobalConfigurationTest {

    MockEnvironmentVariables environmentVariables;

    PTAFConfiguration configuration;

    @Before
    public void initMocks() {
        environmentVariables = new MockEnvironmentVariables();
        configuration = new SystemPropertiesPTAFConfiguration(environmentVariables);
    }

    @Test
    public void the_name_value_can_be_defined_in_a_system_property() {
        environmentVariables.setProperty("ptaf.name", "Ptaf awesome");

        assertThat(configuration.getName(), is("Ptaf awesome"));
    }
}
