package com.project.ptaf.jmx.client;

import com.project.core.endpoint.AbstractEndpoint;
import com.project.core.exceptions.PTAFRuntimeException;
import com.project.core.message.Message;
import com.project.core.messaging.JmxProducer;
import com.project.ptaf.jmx.endpoint.JmxEndpointConfiguration;
import com.project.ptaf.jmx.message.JmxMessage;
import com.project.ptaf.jmx.model.ManagedBeanInvocation;
import com.project.ptaf.jmx.model.ManagedBeanResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.management.JMException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.Collections;

public class JmxClient extends AbstractEndpoint implements JmxProducer {

    private static Logger log = LoggerFactory.getLogger(JmxClient.class);

    private String connectionId;

    public JmxClient() {
        this(new JmxEndpointConfiguration());
    }

    public JmxClient(JmxEndpointConfiguration endpointConfiguration) {
        super(endpointConfiguration);
    }

    @Override
    public JmxEndpointConfiguration getEndpointConfiguration() {
        return (JmxEndpointConfiguration) super.getEndpointConfiguration();
    }

    @Override
    public Message send(Message message) {
        MBeanServerConnection serverConnection;
        if (getEndpointConfiguration().getServerUrl().equals("platform")) {
            serverConnection = ManagementFactory.getPlatformMBeanServer();
        } else {
            serverConnection = getNettworkConnection();
        }

        if (log.isDebugEnabled()) {
            log.debug("Sending message to JMX MBeanServer server: '" + getEndpointConfiguration().getServerUrl() + "'");
            log.debug("Message to send: \n" + message.getPayload(String.class));
        }

        ManagedBeanInvocation invocation = getEndpointConfiguration().getMessageConverte().convertoutbound(message, getEndpointConfiguration());

        ObjectName objectName;
        try {
            if (StringUtils.hasText(invocation.getMbean())) {
                objectName = new ObjectName(invocation.getMbean());
            } else if (StringUtils.hasText(invocation.getObjectKey())) {
                objectName = new ObjectName(invocation.getObjectDomain(), invocation.getObjectKey(), invocation.getObjectValue());
            } else {
                objectName = new ObjectName(invocation.getObjectDomain(), "name", invocation.getObjectValue());
            }
        } catch (MalformedObjectNameException e) {
            throw new PTAFRuntimeException("Failed to create object name ", e);
        }

        try {
            if (invocation.getOperation() != null) {
                Object result = serverConnection.invoke(objectName, invocation.getOperation().getName(), invocation.getOperation().getParamValues(), invocation.getOperation().getParamTypes());
                return JmxMessage.result(result);
            } else if (invocation.getAttribute() != null) {
                ManagedBeanInvocation.Attribute attribute = invocation.getAttribute();

                Object attributeValue = serverConnection.getAttribute(objectName, attribute.getName());

                if (StringUtils.hasText(attribute.getInnerPath())) {
                    if (attribute instanceof CompositeData) {
                        if (!((CompositeData) attributeValue).containsKey(attribute.getInnerPath())) {
                            throw new PTAFRuntimeException(" Failed to find inner path attribute value: " + attribute.getInnerPath());
                        }
                        attributeValue = ((CompositeData) attributeValue).get(attribute.getInnerPath());
                    } else {
                        throw new PTAFRuntimeException("Failed to get inner path on attribute value: " + attributeValue);
                    }
                }
                return JmxMessage.result(attributeValue);
            }
        } catch (JMException | IOException e) {
            throw new PTAFRuntimeException("Failed to execute MBean operation", e);
        }
        return JmxMessage.result();
    }

    public ManagedBeanResult getManagedBeanResult(Message message) {
        return getEndpointConfiguration().getMessageConverte()
                .getBeanResults(message, getEndpointConfiguration());
    }

    private MBeanServerConnection getNettworkConnection() {
        try {
            JMXServiceURL url = new JMXServiceURL(getEndpointConfiguration().getServerUrl());
            String[] creds = {getEndpointConfiguration().getUsername(), getEndpointConfiguration().getPassword()};
            JMXConnector networkConnector = JMXConnectorFactory.connect(url, Collections.singletonMap(JMXConnector.CREDENTIALS, creds));
            connectionId = networkConnector.getConnectionId();

            return networkConnector.getMBeanServerConnection();
        } catch (IOException e) {
            throw new PTAFRuntimeException(" failed to connect to network MBean server", e);
        }
    }

    public String getConnectionId() {
        return connectionId;
    }
}
