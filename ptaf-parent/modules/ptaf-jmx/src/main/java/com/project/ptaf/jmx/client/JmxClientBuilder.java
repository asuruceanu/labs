package com.project.ptaf.jmx.client;

import com.project.core.endpoint.AbstractEndpointBuilder;

public class JmxClientBuilder extends AbstractEndpointBuilder<JmxClient> {

    private JmxClient endpoint = new JmxClient();

    @Override
    protected JmxClient getEndpoint() {
        return endpoint;
    }

    public JmxClientBuilder serverUrl(String serverUrl) {
        endpoint.getEndpointConfiguration().setServerUrl(serverUrl);
        return this;
    }

    public JmxClientBuilder username(String username) {
        endpoint.getEndpointConfiguration().setUsername(username);
        return this;
    }

    public JmxClientBuilder password(String password) {
        endpoint.getEndpointConfiguration().setPassword(password);
        return this;
    }
}
