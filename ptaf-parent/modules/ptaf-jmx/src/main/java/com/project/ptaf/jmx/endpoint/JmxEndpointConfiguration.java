package com.project.ptaf.jmx.endpoint;

import com.project.core.endpoint.AbstractEndpointConfiguration;
import com.project.ptaf.jmx.message.JmxMessageConverter;
import com.project.ptaf.jmx.model.JmxMarshaller;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.StringUtils;

import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Map;

public class JmxEndpointConfiguration extends AbstractEndpointConfiguration implements ApplicationContextAware {

    private String serverUrl;

    private String protocol = "rmi";
    private String host = "localhost";
    private int port = Registry.REGISTRY_PORT;
    private String binding;


    private String username;
    private String password;

    private JmxMarshaller marshaller = new JmxMarshaller();

    private JmxMessageConverter messageConverte = new JmxMessageConverter();

    private Map<String, Object> enviromentProperties = new HashMap<>();

    private ApplicationContext applicationContext;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getBinding() {
        return binding;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    public String getServerUrl() {
        if (StringUtils.hasText(this.serverUrl)) {
            return serverUrl;
        } else {
            return "service:jmx:" + protocol + ":///jidndi/" + protocol + "://" + host + ":" + port + (binding != null ? "/" + binding : "");
        }
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JmxMarshaller getMarshaller() {
        return marshaller;
    }

    public void setMarshaller(JmxMarshaller marshaller) {
        this.marshaller = marshaller;
    }

    public Map<String, Object> getEnviromentProperties() {
        return enviromentProperties;
    }

    public void setEnviromentProperties(Map<String, Object> enviromentProperties) {
        this.enviromentProperties = enviromentProperties;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public JmxMessageConverter getMessageConverte() {
        return messageConverte;
    }


}
