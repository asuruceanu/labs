package com.project.ptaf.jmx.message;

import com.project.core.exceptions.PTAFRuntimeException;
import com.project.core.message.DefaultMessage;
import com.project.ptaf.jmx.model.JmxMarshaller;
import com.project.ptaf.jmx.model.ManagedBeanResult;
import com.project.ptaf.jmx.model.ManagedBeanInvocation;
import com.project.ptaf.jmx.model.OperationParam;
import org.springframework.xml.transform.StringResult;

public class JmxMessage extends DefaultMessage {

    private ManagedBeanInvocation managedBeanInvocation;
    private ManagedBeanResult managedBeanResult;

    private JmxMarshaller marshaller = new JmxMarshaller();

    private JmxMessage() {
        super();
    }

    private JmxMessage(ManagedBeanInvocation managedBeanInvocation) {
        super(managedBeanInvocation);
        this.managedBeanInvocation = managedBeanInvocation;
    }

    private JmxMessage(ManagedBeanResult managedBeanResult) {
        super(managedBeanResult);
        this.managedBeanResult = managedBeanResult;
    }

    public static JmxMessage invocation(String mbean) {
        ManagedBeanInvocation invocation = new ManagedBeanInvocation();
        invocation.setMbean(mbean);

        return new JmxMessage(invocation);
    }

    public static JmxMessage invocation(String objectDomain, String objectName) {
        ManagedBeanInvocation invocation = new ManagedBeanInvocation();
        invocation.setObjectDomain(objectDomain);
        invocation.setObjectName(objectName);

        return new JmxMessage(invocation);
    }

    public static JmxMessage invocation(String objectDomain, String objectKey, String objectValue) {
        ManagedBeanInvocation invocation = new ManagedBeanInvocation();
        invocation.setObjectDomain(objectDomain);
        invocation.setObjectKey(objectKey);
        invocation.setObjectValue(objectValue);

        return new JmxMessage(invocation);
    }

    public static JmxMessage result(Object value) {
        ManagedBeanResult mbeanResult = new ManagedBeanResult();
        ManagedBeanResult.Object mbeanResultObject = new ManagedBeanResult.Object();
        mbeanResultObject.setValueObject(value);
        mbeanResult.setObject(mbeanResultObject);

        return new JmxMessage(mbeanResult);
    }

    public static JmxMessage result() {
        return new JmxMessage(new ManagedBeanResult());
    }

    public static JmxMessage invocation() {
        return new JmxMessage(new ManagedBeanInvocation());
    }

    public JmxMessage attribute(String name) {
        return attribute(name, null, null);
    }

    public JmxMessage attribute(String name, Object value) {
        return attribute(name, value, value.getClass());
    }

    public JmxMessage attribute(String name, Object value, Class<?> valueType) {
        if (managedBeanInvocation == null) {
            throw new PTAFRuntimeException("invalid access to attribute for JMX message");
        }

        ManagedBeanInvocation.Attribute attribute = new ManagedBeanInvocation.Attribute();
        attribute.setName(name);
        if (value != null) {
            attribute.setValueObject(value);
            attribute.setType(valueType.getName());
        }

        managedBeanInvocation.setAttribute(attribute);
        return this;
    }

    public JmxMessage operation(String name) {
        if (managedBeanInvocation == null) {
            throw new PTAFRuntimeException("invalid access to attribute for JMX message");
        }

        ManagedBeanInvocation.Operation operation = new ManagedBeanInvocation.Operation();
        operation.setName(name);
        managedBeanInvocation.setOperation(operation);
        return this;
    }

    public JmxMessage parameter(Object arg) {
        return parameter(arg, arg.getClass());
    }

    public JmxMessage parameter(Object arg, Class<?> argType) {
        if (managedBeanInvocation == null) {
            throw new PTAFRuntimeException("invalid access to operation parameter for JMX message");
        }

        if (managedBeanInvocation.getOperation() == null) {
            throw new PTAFRuntimeException("invalid access to operation parameter before operation was set for JMX message");
        }

        if (managedBeanInvocation.getOperation().getParameter() == null) {
            managedBeanInvocation.getOperation().setParameter(new ManagedBeanInvocation.Parameter());
        }

        OperationParam operationParam = new OperationParam();
        operationParam.setValueObject(arg);
        operationParam.setType(argType.getName());
        managedBeanInvocation.getOperation().getParameter().getParameter().add(operationParam);
        return this;
    }

    public JmxMessage parameter(Object arg, String type) {
        if (managedBeanInvocation == null) {
            throw new PTAFRuntimeException("invalid access to operation parameter for JMX message");
        }

        if (managedBeanInvocation.getOperation() == null) {
            throw new PTAFRuntimeException("invalid access to operation parameter before operation was set for JMX message");
        }

        if (managedBeanInvocation.getOperation().getParameter() == null) {
            managedBeanInvocation.getOperation().setParameter(new ManagedBeanInvocation.Parameter());
        }

        OperationParam operationParam = new OperationParam();
        operationParam.setValueObject(arg);
        operationParam.setType(type);
        managedBeanInvocation.getOperation().getParameter().getParameter().add(operationParam);
        return this;
    }

    @Override
    public <T> T getPayload(Class<T> type) {
        if (String.class.equals(type)) {
            return (T) getPayload();
        } else {
            return super.getPayload(type);
        }
    }

    @Override
    public Object getPayload() {
        StringResult payloadResult = new StringResult();
        if (managedBeanInvocation != null) {
            marshaller.marshal(managedBeanInvocation, payloadResult);
            return payloadResult.toString();
        } else if (managedBeanResult != null) {
            marshaller.marshal(managedBeanResult, payloadResult);
            return payloadResult.toString();
        }

        return super.getPayload();
    }

}
