package com.project.ptaf.jmx.message;

import com.project.core.message.Message;
import com.project.ptaf.jmx.endpoint.JmxEndpointConfiguration;
import com.project.ptaf.jmx.model.ManagedBeanInvocation;
import com.project.ptaf.jmx.model.ManagedBeanResult;
import org.springframework.util.StringUtils;

import javax.xml.transform.Source;

public class JmxMessageConverter {

    public ManagedBeanInvocation convertoutbound(Message internalMessage, JmxEndpointConfiguration endpointConfiguration) {
        return getServiceInvocation(internalMessage, endpointConfiguration);
    }

    private ManagedBeanInvocation getServiceInvocation(Message message, JmxEndpointConfiguration endpointConfiguration) {
        Object payload = message.getPayload();

        ManagedBeanInvocation serviceInvocation = null;
        if (payload != null) {
            if (payload instanceof ManagedBeanInvocation) {
                serviceInvocation = (ManagedBeanInvocation) payload;
            } else if (StringUtils.hasText(message.getPayload(String.class))) {
                serviceInvocation = (ManagedBeanInvocation) endpointConfiguration.getMarshaller()
                        .unmarshal(message.getPayload(Source.class));
            } else {
                serviceInvocation = new ManagedBeanInvocation();
            }
        }
        return serviceInvocation;
    }

    public ManagedBeanResult getBeanResults(Message message, JmxEndpointConfiguration endpointConfiguration) {
        Object payload = message.getPayload();

        ManagedBeanResult managedBeanResult = null;
        if (payload != null) {
            if (payload instanceof ManagedBeanResult) {
                managedBeanResult = (ManagedBeanResult) payload;
            } else if (StringUtils.hasText(message.getPayload(String.class))) {
                managedBeanResult = (ManagedBeanResult) endpointConfiguration.getMarshaller()
                        .unmarshal(message.getPayload(Source.class));
            } else {
                managedBeanResult = new ManagedBeanResult();
            }
        }
        return managedBeanResult;
    }
}
