package com.project.ptaf.jmx.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

public class JmxMarshaller extends Jaxb2Marshaller {

    private static Logger log = LoggerFactory.getLogger(JmxMarshaller.class);

    public JmxMarshaller() {
        setClassesToBeBound(ManagedBeanInvocation.class, ManagedBeanInvocation.class);

        setSchema(new ClassPathResource("com/project/schema/ptaf-jmx-message.xsd"));

        try {
            afterPropertiesSet();
        } catch (Exception e) {
            log.warn("Failed to setup jmx message marshaller", e);
        }
    }
}
