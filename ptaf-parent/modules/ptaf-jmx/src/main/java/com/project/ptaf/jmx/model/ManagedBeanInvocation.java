package com.project.ptaf.jmx.model;


import com.project.core.exceptions.PTAFRuntimeException;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.SimpleTypeConverter;

import javax.xml.bind.annotation.*;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "mbean",
        "objectDomain",
        "objectName",
        "objectKey",
        "objectValue",
        "attribute",
        "operation"
})
@XmlRootElement(name = "mbean-invocation")
public class ManagedBeanInvocation {

    @XmlElement
    protected String mbean;
    @XmlElement
    protected String objectDomain;
    @XmlElement
    protected String objectName;
    @XmlElement
    protected String objectKey;
    @XmlElement
    protected String objectValue;

    @XmlElement
    protected ManagedBeanInvocation.Attribute attribute;

    @XmlElement
    protected ManagedBeanInvocation.Operation operation;

    public Object getAttributeValue() {
        if (attribute == null) {
            return null;
        }

        if (attribute.getValueObject() != null) {
            return attribute.getValueObject();
        }

        try {
            Class argType = Class.forName(attribute.getType());
            Object value = null;

            if (attribute.getValue() != null) {
                value = attribute.getValue();
            }

            if (value == null) {
                return null;
            } else if (argType.isInstance(value) || argType.isAssignableFrom(value.getClass())) {
                return argType.cast(value);
            } else if (Map.class.equals(argType)) {
                String mapString = value.toString();

                Properties props = new Properties();
                try {
                    props.load(new StringReader(mapString.substring(1, mapString.length() - 1).replace(", ", "\n")));
                } catch (IOException e) {
                    throw new PTAFRuntimeException("Failed to reconstruct attribute object of type map", e);
                }
                Map<String, String> map = new LinkedHashMap<>();
                for (Map.Entry<Object, Object> entry : props.entrySet()) {
                    map.put(entry.getKey().toString(), entry.getValue().toString());
                }

                return map;
            } else {
                try {
                    return new SimpleTypeConverter().convertIfNecessary(value, argType);
                } catch (ConversionNotSupportedException e) {
                    if (String.class.equals(argType)) {
                        return value.toString();
                    }
                    throw e;
                }
            }
        } catch (ClassNotFoundException e) {
            throw new PTAFRuntimeException("Failed to construct attribute object", e);
        }
    }

    public String getMbean() {
        return mbean;
    }

    public void setMbean(String mbean) {
        this.mbean = mbean;
    }

    public String getObjectDomain() {
        return objectDomain;
    }

    public void setObjectDomain(String objectDomain) {
        this.objectDomain = objectDomain;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }

    public String getObjectValue() {
        return objectValue;
    }

    public void setObjectValue(String objectValue) {
        this.objectValue = objectValue;
    }

    public ManagedBeanInvocation.Operation getOperation() {
        return operation;
    }

    public void setOperation(ManagedBeanInvocation.Operation operation) {
        this.operation = operation;
    }

    public ManagedBeanInvocation.Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(ManagedBeanInvocation.Attribute attribute) {
        this.attribute = attribute;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Attribute {

        @XmlAttribute(name = "name")
        protected String name;
        @XmlAttribute(name = "type")
        protected String type = String.class.getName();
        @XmlAttribute(name = "value")
        protected String value;
        @XmlAttribute(name = "ref")
        protected String ref;
        @XmlAttribute(name = "inner-path")
        protected String innerPath;

        @XmlTransient
        private Object valueObject;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getRef() {
            return ref;
        }

        public void setRef(String ref) {
            this.ref = ref;
        }

        public String getInnerPath() {
            return innerPath;
        }

        public void setInnerPath(String innerPath) {
            this.innerPath = innerPath;
        }

        public Object getValueObject() {
            return valueObject;
        }

        public void setValueObject(Object valueObject) {
            setType(valueObject.getClass().getName());
            setValue(valueObject.toString());
            this.valueObject = valueObject;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Operation {

        @XmlAttribute(name = "name")
        protected String name;
        @XmlAttribute(name = "return-type")
        protected String returnType;

        protected ManagedBeanInvocation.Parameter parameter;

        public String[] getParamTypes() {
            List<String> types = new ArrayList<>();

            if (parameter != null) {
                for (OperationParam arg : parameter.getParameter()) {
                    types.add(arg.getType());
                }
            }
            return types.toArray(new String[types.size()]);
        }

        public Object[] getParamValues() {
            List<Object> argValues = new ArrayList<>();


            try {
                if (parameter != null) {
                    for (OperationParam operationParam : parameter.getParameter()) {
                        Class argType = Class.forName(operationParam.getType());
                        Object value = null;

                        if (operationParam.getValueObject() != null) {
                            value = operationParam.getValueObject();
                        } else if (operationParam.getValue() != null) {
                            value = operationParam.getValue();
                        }

                        if (value == null) {
                            argValues.add(null);
                        } else if (Map.class.equals(argType)) {
                            String mapString = value.toString();

                            Properties props = new Properties();
                            try {
                                props.load(new StringReader(mapString.substring(1, mapString.length() - 1).replace(", ", "\n")));
                            } catch (IOException e) {
                                throw new PTAFRuntimeException("Failed to reconstruct attribute object of type map", e);
                            }
                            Map<String, String> map = new LinkedHashMap<>();
                            for (Map.Entry<Object, Object> entry : props.entrySet()) {
                                map.put(entry.getKey().toString(), entry.getValue().toString());
                            }

                            argValues.add(map);
                        } else {
                            try {
                                argValues.add(new SimpleTypeConverter().convertIfNecessary(value, argType));
                            } catch (ConversionNotSupportedException e) {
                                if (String.class.equals(argType)) {
                                    argValues.add(value.toString());
                                }
                                throw e;
                            }
                        }
                    }
                }
            } catch (ClassNotFoundException e) {
                throw new PTAFRuntimeException("Failed to construct attribute object", e);
            }
            return argValues.toArray(new Object[argValues.size()]);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getReturnType() {
            return returnType;
        }

        public void setReturnType(String returnType) {
            this.returnType = returnType;
        }

        public ManagedBeanInvocation.Parameter getParameter() {
            return parameter;
        }

        public void setParameter(ManagedBeanInvocation.Parameter parameter) {
            this.parameter = parameter;
        }
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = "parameter")
    public static class Parameter {

        @XmlElement(name = "param", required = true)
        protected List<OperationParam> parameter;

        public List<OperationParam> getParameter() {
            if (parameter == null) {
                parameter = new ArrayList<OperationParam>();
            }
            return this.parameter;
        }
    }
}
