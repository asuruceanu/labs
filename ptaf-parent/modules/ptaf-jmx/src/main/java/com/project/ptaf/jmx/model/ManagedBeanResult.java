package com.project.ptaf.jmx.model;

import com.project.core.exceptions.PTAFRuntimeException;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.SimpleTypeConverter;

import javax.xml.bind.annotation.*;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"object"})
@XmlRootElement(name = "mbean-result")
public class ManagedBeanResult {

    @XmlElement
    protected ManagedBeanResult.Object object;

    public ManagedBeanResult.Object getObject() {
        return object;
    }

    public void setObject(ManagedBeanResult.Object value) {
        this.object = value;
    }

    public java.lang.Object getResultObject() {
        if (object == null) {
            return null;
        }

        if (object.getValueObject() != null) {
            return object.getValueObject();
        }

        try {
            Class argType = Class.forName(object.getType());
            java.lang.Object value = null;

            if (object.getValue() != null) {
                return null;
            } else if (argType.isInstance(value) || argType.isAssignableFrom(value.getClass())) {
                return argType.cast(value);
            } else if (Map.class.equals(argType)) {
                String mapString = value.toString();

                Properties props = new Properties();
                try {
                    props.load(new StringReader(mapString.substring(1, mapString.length() - 1).replace(", ", "\n")));
                } catch (IOException e) {
                    throw new PTAFRuntimeException("Failed to reconstruct attribute object of type map", e);
                }
                Map<String, String> map = new LinkedHashMap<>();
                for (Map.Entry<java.lang.Object, java.lang.Object> entry : props.entrySet()) {
                    map.put(entry.getKey().toString(), entry.getValue().toString());
                }

                return map;
            } else {
                try {
                    return new SimpleTypeConverter().convertIfNecessary(value, argType);
                } catch (ConversionNotSupportedException e) {
                    if (String.class.equals(argType)) {
                        return value.toString();
                    }
                    throw e;
                }
            }
        } catch (ClassNotFoundException e) {
            throw new PTAFRuntimeException("Failed to construct service result object", e);
        }
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Object {

        @XmlAttribute(name = "type")
        protected String type = String.class.getName();
        @XmlAttribute(name = "value")
        protected String value;
        @XmlAttribute(name = "ref")
        protected String ref;

        @XmlTransient
        private java.lang.Object valueObject;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getRef() {
            return ref;
        }

        public void setRef(String ref) {
            this.ref = ref;
        }

        public java.lang.Object getValueObject() {
            return valueObject;
        }

        public void setValueObject(java.lang.Object valueObject) {
            setType(valueObject.getClass().getName());
            setValue((valueObject.toString()));
            this.valueObject = valueObject;
        }
    }
}
