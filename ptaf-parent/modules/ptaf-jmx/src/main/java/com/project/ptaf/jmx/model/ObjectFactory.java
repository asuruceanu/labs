package com.project.ptaf.jmx.model;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public ManagedBeanInvocation createmanagedBeanInvocation() {
        return new ManagedBeanInvocation();
    }

    public ManagedBeanInvocation.Parameter craeteManagedBeanInvocationParameter() {
        return new ManagedBeanInvocation.Parameter();
    }

    public OperationParam createOperationParam() {
        return new OperationParam();
    }

    public ManagedBeanResult createMangedBeanResult() {
        return new ManagedBeanResult();
    }

    public ManagedBeanResult.Object createmanagedBeanResultObject() {
        return new ManagedBeanResult.Object();
    }
}
