package com.project.ptaf.jmx.model;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationParam")
public class OperationParam {

    @XmlAttribute(name = "type")
    protected String type = String.class.getName();
    @XmlAttribute(name = "value")
    protected String value;
    @XmlAttribute(name = "ref")
    protected String ref;

    @XmlTransient
    private Object valueObject;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Object getValueObject() {
        return valueObject;
    }

    public void setValueObject(Object valueObject) {
        setType(valueObject.getClass().toString());
        setValue(valueObject.toString());
        this.valueObject = valueObject;
    }
}
