package com.project.integration;

import com.j256.simplejmx.server.JmxServer;
import com.project.mbean.HelloBean;
import com.project.mbean.HelloBeanImpl;
import com.project.ptaf.jmx.client.JmxClient;
import com.project.ptaf.jmx.client.JmxClientBuilder;
import com.project.ptaf.jmx.message.JmxMessage;
import com.project.ptaf.jmx.model.ManagedBeanResult;
import org.junit.*;

import javax.management.JMException;
import java.lang.management.ManagementFactory;

public class jmxClientIT {

    private JmxServer jmxServer;
    private JmxClient jmxClient;
    private final String serverUrl = "platform";

    @Before
    public void startJmxServerWithManagedBeans() throws JMException{
        jmxServer = new JmxServer(ManagementFactory.getPlatformMBeanServer());

        jmxServer.start();

        HelloBean helloBean = new HelloBeanImpl();

        jmxServer.register(helloBean);

        jmxClient = new JmxClientBuilder()
                .serverUrl(serverUrl)
                .build();
    }

    @After
    public void stopJmxServer() { jmxServer.stop(); }

    @Test
    @Ignore
    public void jmxClientInvokeHelloBeanAttributeTest() throws JMException {

        JmxMessage helloJmxMessage = JmxMessage.invocation("com.project.mbean:name=HelloBean")
                .attribute("helloMessage", "Project");

        JmxMessage expectedHelloJmxResults = JmxMessage.result("Project");
        ManagedBeanResult expectedHelloBean = jmxClient.getManagedBeanResult(expectedHelloJmxResults);

        JmxMessage actualHelloJmxMessageResult = (JmxMessage) jmxClient.send(helloJmxMessage);
        ManagedBeanResult actualHelloBean = jmxClient.getManagedBeanResult(actualHelloJmxMessageResult);

        System.out.println("Expected results: " +expectedHelloJmxResults.getPayload(String.class));
        System.out.println("Actual results: " +actualHelloJmxMessageResult.getPayload(String.class));

        Assert.assertEquals(expectedHelloBean.getResultObject(), actualHelloBean.getResultObject());
    }

    @Test
    public void jmxClientInvokeHelloBeanOperationsTest() throws JMException {

        JmxMessage helloJmxMessage = JmxMessage.invocation("com.project.mbean:name=HelloBean")
                .operation("hello")
                .parameter("Project");

        JmxMessage expectedHelloJmxResults = JmxMessage.result("Project");
        ManagedBeanResult expectedHelloBean = jmxClient.getManagedBeanResult(expectedHelloJmxResults);

        JmxMessage actualHelloJmxMessageResult = (JmxMessage) jmxClient.send(helloJmxMessage);
        ManagedBeanResult actualHelloBean = jmxClient.getManagedBeanResult(actualHelloJmxMessageResult);

        System.out.println("Expected results: " +expectedHelloJmxResults.getPayload(String.class));
        System.out.println("Actual results: " +actualHelloJmxMessageResult.getPayload(String.class));

        Assert.assertEquals(expectedHelloBean.getResultObject(), actualHelloBean.getResultObject());
    }
}
