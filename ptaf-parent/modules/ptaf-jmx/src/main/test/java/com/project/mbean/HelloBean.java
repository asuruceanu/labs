package com.project.mbean;

public interface HelloBean {

    String getHelloMessage();

    void setHelloMessage(String message);

    String hello(String username);
}
