package com.project.mbean;

import com.j256.simplejmx.common.JmxAttributeField;
import com.j256.simplejmx.common.JmxResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JmxResource(description = "hello bean", domainName = "com.project.mbean", beanName = "HelloBean")
public class HelloBeanImpl implements HelloBean {

    private static Logger log = LoggerFactory.getLogger(HelloBeanImpl.class);


    @JmxAttributeField(description = "helloMessage attribute field", isWritable = true)
    private String helloMessage = "Hello %s this is cool";

    @Override
    public String getHelloMessage() {
        return helloMessage;
    }

    @Override
    public void setHelloMessage(String message) {
        this.helloMessage = message;
    }

    @Override
    public String hello(String username) {
        log.info(String.format(helloMessage, username));
        return String.format(helloMessage, username);
    }
}
